package com.we.gilwe;

import com.we.gilwe.api.ApiService;
import com.we.gilwe.api.Factory;
import com.we.gilwe.model.BaseModel;
import com.we.gilwe.model.RoadModel;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import retrofit2.http.GET;
import rx.Observable;

import static com.we.gilwe.utils.MultipartUtils.createImagePart;
import static com.we.gilwe.utils.MultipartUtils.createPartFromString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by moltak on 2/24/17.
 */

public class GilweApiTest {
    private MockWebServer server;
    private String host;

    @Before
    public void setup() throws IOException {
        server = new MockWebServer();
        server.setDispatcher(new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                switch (request.getPath()) {
                    case "/api/v1/spot":
                        return new MockResponse().setBody("{\"result\":\"ok\"}");
                }
                return null;
            }
        });
        server.start();
        host = server.url("/api/v1/").url().toString();
    }

    @Test
    public void shouldReturnTrueWhenFileSendingUsingMultipart()
            throws ExecutionException, InterruptedException {

        String file = "/home/moltak/Downloads/1.png";

        BaseModel r = Factory.getRetrofit("http://gilwe.com:3000/").create(ApiService.class)
                .spot(createPartFromString("1"),
                        createPartFromString("1"),
                        createPartFromString("text"),
                        null,
                        createImagePart(file))
                .toBlocking()
                .toFuture()
                .get();

        assertThat(r.getResult(), is("ok"));
    }

    @Test
    public void shouldReturn_road0_With_5spots() throws ExecutionException, InterruptedException {
        RoadModel r = Factory.getRetrofit("https://raw.githubusercontent.com/moltak/gilwe_sample_data/master/sample_data/")
                .create(GetRoad0.class)
                .get()
                .toBlocking()
                .toFuture()
                .get();

        assertThat(r.getSpots().size(), is(5));
        assertThat(r.getId(), is(0));
        assertThat(r.getText(), is("사랑할때 걷기 좋은길"));
        assertThat(r.getAuthor(), is("세일"));
    }

    interface GetRoad0 {
        @GET("road_0.json")
        Observable<RoadModel> get();
    }
}
