package com.we.gilwe.ui.road;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.we.gilwe.R;
import com.we.gilwe.databinding.ActivityFollowingBinding;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.ui.common.BaseActivity;
import com.we.gilwe.ui.common.Constants;
import com.we.gilwe.ui.detail.RoadDetailContract;
import com.we.gilwe.ui.road.adapter.RoadFollowAdapter;
import com.we.gilwe.ui.road.presenter.RoadFollowPresenter;
import com.we.gilwe.utils.GoogleMapRouteUtils;

import net.daum.mf.map.api.MapView;

import java.util.concurrent.ExecutionException;

public class RoadFollowActivity extends BaseActivity implements RoadDetailContract.View, OnMapReadyCallback {

    private ActivityFollowingBinding binding;
    private RoadFollowAdapter adapter;
    private RoadDetailContract.Presenter presenter;
    private int roadId;
    private GoogleMap googleMap;

    public static Intent createIntent(Context context, int id) {
        Intent intent = new Intent(context, RoadFollowActivity.class);
        intent.putExtra(Constants.EXTRA_ID, id);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_following);
        roadId = getIntent().getIntExtra(Constants.EXTRA_ID, -1);
        initToolbar();
        initView();
        presenter = new RoadFollowPresenter();
        presenter.attachView(this);
        presenter.loadRoad(getIntent().getIntExtra(Constants.EXTRA_ID, -1));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initToolbar() {
        setSupportActionBar(binding.toolbar);

        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle(null);
        }
    }

    private void initView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void addStickyHeader() {
        // Add the sticky headers decoration
        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
        binding.recyclerView.addItemDecoration(headersDecor);
    }

    @Override
    public void showRoad(RoadModel roadModel) {
        adapter = new RoadFollowAdapter(this, roadModel, presenter);
        binding.recyclerView.setAdapter(adapter);

        binding.toolbarTitle.setText(roadModel.getTitle());

        addStickyHeader();
        try {
            GoogleMapRouteUtils.drawRouteFlag(roadModel, googleMap);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setMyLocationEnabled(true);
    }
}
