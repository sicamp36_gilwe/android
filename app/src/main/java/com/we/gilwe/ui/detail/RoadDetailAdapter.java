package com.we.gilwe.ui.detail;

import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;
import com.we.gilwe.R;
import com.we.gilwe.databinding.DetailHeaderLayoutBinding;
import com.we.gilwe.databinding.DetailImageItemLayoutBinding;
import com.we.gilwe.databinding.DetailSectionLayoutBinding;
import com.we.gilwe.databinding.DetailTextItemLayoutBinding;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SpotModel;
import com.we.gilwe.ui.detail.viewmodel.RoadDetailHeaderViewModel;
import com.we.gilwe.ui.detail.viewmodel.RoadImageItemViewModel;
import com.we.gilwe.ui.detail.viewmodel.RoadSectionViewModel;
import com.we.gilwe.ui.detail.viewmodel.RoadTextItemViewModel;
import com.we.gilwe.ui.view.recyclerview.SimpleViewHolder;
import com.we.gilwe.utils.Validator;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class RoadDetailAdapter extends RecyclerView.Adapter<SimpleViewHolder> implements StickyRecyclerHeadersAdapter<SimpleViewHolder> {
    private static final int VIEW_TYPE_HEADER = 1;
    private static final int VIEW_TYPE_IMAGE_ITEM = 2;
    private static final int VIEW_TYPE_TEXT_ITEM = 3;

    private RoadModel roadModel;
    //    private List<SpotModel> spotModelList = new ArrayList<>();
//    private Map<Integer, List<SpotModel>> spotListMap = new TreeMap<>();
    private List<Pair<Integer, List<SpotModel>>> spotPairList = new ArrayList<>();

    private RoadDetailContract.Presenter presenter;

    RoadDetailAdapter(RoadModel roadModel, RoadDetailContract.Presenter presenter) {
        this.roadModel = roadModel;
        this.presenter = presenter;
//        this.spotModelList = roadModel.getSpots().get(0);
        makeGroupSpotModel(roadModel.getSpots());
    }

    private void makeGroupSpotModel(List<List<SpotModel>> spotModelList) {
        int index = 1;  //header
        for (int i = 0; i < spotModelList.size(); i++) {
            List<SpotModel> spotModels = spotModelList.get(i);

            try {
                //remove empty attachment spot
                List<SpotModel> result = Observable.from(spotModels)
                        .filter(spotModel -> spotModel != null && spotModel.getSpotType() != SpotModel.SpotType.UNKNOWN)
                        .toList()
                        .filter(Validator::isNotEmpty)
                        .toBlocking().toFuture().get();
                spotPairList.add(Pair.create(index, result));

//                List<Integer> integers = Observable.from(spotListMap.keySet())
//                        .toSortedList().toBlocking().toFuture().get();
//                List<Integer> keys = new ArrayList<>();
//                for (Integer key : spotListMap.keySet()) {
//                    keys.add(key);
//                }
                index += spotModels.size();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SimpleViewHolder viewHolder = null;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                viewHolder = SimpleViewHolder.create(R.layout.detail_header_layout, parent);
                break;
            case VIEW_TYPE_IMAGE_ITEM:
                viewHolder = SimpleViewHolder.create(R.layout.detail_image_item_layout, parent);
                break;
            case VIEW_TYPE_TEXT_ITEM:
                viewHolder = SimpleViewHolder.create(R.layout.detail_text_item_layout, parent);
                break;

        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        if (holder == null) {
            return;
        }

        int itemViewType = getItemViewType(position);
        switch (itemViewType) {
            case VIEW_TYPE_HEADER:
                DetailHeaderLayoutBinding headerBinding = (DetailHeaderLayoutBinding) holder.getViewDataBinding();
                headerBinding.setViewModel(new RoadDetailHeaderViewModel(headerBinding.getRoot(), roadModel, presenter));
                break;
            case VIEW_TYPE_IMAGE_ITEM:
                DetailImageItemLayoutBinding imageBinding = (DetailImageItemLayoutBinding) holder.getViewDataBinding();
                imageBinding.setViewModel(new RoadImageItemViewModel(getSpotModel(position), presenter));
                break;
            case VIEW_TYPE_TEXT_ITEM:
                DetailTextItemLayoutBinding textBinding = (DetailTextItemLayoutBinding) holder.getViewDataBinding();
                textBinding.setViewModel(new RoadTextItemViewModel(getSpotModel(position), presenter));
                break;
        }
    }

    private boolean isFirstItemInSection(int position) {
        position--;
        SpotModel spotModel = null;
        for (Pair<Integer, List<SpotModel>> pair : spotPairList) {
            List<SpotModel> spotModels = pair.second;
            int size = spotModels.size();
            if (position < size) {
                return position == 0;
            } else {
                position -= size;
            }
        }
        return false;
    }

    @Nullable
    private SpotModel getSpotModel(int position) {
        position--;
        SpotModel spotModel = null;
        for (Pair<Integer, List<SpotModel>> pair : spotPairList) {
            List<SpotModel> spotModels = pair.second;
            int size = spotModels.size();
            if (position < size) {
                spotModel = spotModels.get(position);
                break;
            } else {
                position -= size;
            }
        }
        return spotModel;
    }

    private Integer getKey(int position) {
        position--;
        for (Pair<Integer, List<SpotModel>> pair : spotPairList) {
            List<SpotModel> spotModels = pair.second;
            int size = spotModels.size();
            if (position < size) {
                return pair.first;
            } else {
                position -= size;
            }
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        int count = 1; // header
        if (Validator.isEmpty(spotPairList)) {
            return count;
        }

        for (Pair<Integer, List<SpotModel>> spotPair : spotPairList) {
            List<SpotModel> spotModels = spotPair.second;
            count += spotModels.size();
        }

        return count;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return VIEW_TYPE_HEADER;
        }

        SpotModel spotModel = getSpotModel(position);
        if (spotModel == null) {
            return 1010;
        }

        switch (spotModel.getSpotType()) {
            case IMAGE:
                return VIEW_TYPE_IMAGE_ITEM;
            case TEXT:
                return VIEW_TYPE_TEXT_ITEM;
        }

        return -1;
    }

    @Override
    public long getHeaderId(int position) {
        if (position == 0) {
            return -1;
        }

        return getKey(position);
//        return -1;
    }

    @Override
    public SimpleViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return SimpleViewHolder.create(R.layout.detail_section_layout, parent);
    }

    @Override
    public void onBindHeaderViewHolder(SimpleViewHolder holder, int position) {

        Integer key = getKey(position);
        List<SpotModel> spotModels = null;
        for (Pair<Integer, List<SpotModel>> spotPair : spotPairList) {
            if (key.intValue() == spotPair.first) {
                spotModels = spotPair.second;
            }
        }
        int attachmentCount = 0;
        if (spotModels != null) {
            for (SpotModel spotModel : spotModels) {
                if (spotModel != null && spotModel.getSpotType() != SpotModel.SpotType.UNKNOWN) {
                    attachmentCount++;
                }
            }
        }

        DetailSectionLayoutBinding binding = (DetailSectionLayoutBinding) holder.getViewDataBinding();
        binding.setViewModel(new RoadSectionViewModel(attachmentCount, spotModels.get(0), presenter));
        binding.executePendingBindings();
    }
}
