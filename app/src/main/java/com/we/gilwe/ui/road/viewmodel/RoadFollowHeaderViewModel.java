package com.we.gilwe.ui.road.viewmodel;

import com.we.gilwe.model.RoadModel;
import com.we.gilwe.ui.detail.RoadDetailContract;
import com.we.gilwe.utils.MapRouteUtils;

import net.daum.mf.map.api.MapView;

import java.util.concurrent.ExecutionException;

/**
 * Created by moltak on 2/26/17.
 */

public class RoadFollowHeaderViewModel {
    public RoadFollowHeaderViewModel(
            MapView mapView,
            RoadModel roadModel,
            RoadDetailContract.Presenter presenter) {

        try {
            MapRouteUtils.drawRouteFlag(roadModel, mapView);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
