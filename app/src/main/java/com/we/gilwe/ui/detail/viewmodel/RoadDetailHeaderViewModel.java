package com.we.gilwe.ui.detail.viewmodel;

import android.databinding.BaseObservable;
import android.view.View;

import com.we.gilwe.BaseApplication;
import com.we.gilwe.R;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.model.SpotModel;
import com.we.gilwe.preferences.SettingsPreferences;
import com.we.gilwe.ui.detail.RoadDetailContract;
import com.we.gilwe.utils.ToastHelper;

import java.util.List;

public class RoadDetailHeaderViewModel extends BaseObservable {

    private View root;
    private RoadModel roadModel;
    private RoadDetailContract.Presenter presenter;

    public RoadDetailHeaderViewModel(View root, RoadModel roadModel, RoadDetailContract.Presenter presenter) {
        this.root = root;
        this.roadModel = roadModel;
        this.presenter = presenter;
    }

    public String getImageUrl() {
        //FIXME
        for (int i = 0; i < roadModel.getSpots().size(); i++) {
            List<SpotModel> spotModels = roadModel.getSpots().get(i);

            for (SpotModel spotModel : spotModels) {
                if (spotModel.getSpotType() == SpotModel.SpotType.IMAGE) {
                    return spotModel.getImage();
                }
            }
        }
        return null;
    }

    public String getTitle() {
        return roadModel.getTitle();
    }

    public String getBookMarkCount() {
        return roadModel.getBookMarkCount() + "";
    }

    public String getProfileUrl() {
        return roadModel.getUser().getProfileUrl();
    }

    public boolean isBookMarked() {
        return SettingsPreferences.getInstance(BaseApplication.getContext()).isBookMarked(roadModel.getId());
    }

    public void onClickBookMark(View v) {
//        presenter.onClickBookMark(v, roadModel.getId());
        SettingsPreferences preferences = SettingsPreferences.getInstance(BaseApplication.getContext());

        int count  = roadModel.getBookMarkCount();

        count = preferences.isBookMarked(roadModel.getId()) ? Math.max(count - 1, 0) : count + 1;
        roadModel.setBookMarkCount(count);
        SettingsPreferences.getInstance(BaseApplication.getContext()).setBookMark(SimpleRoadModel.create(roadModel));
        notifyChange();
    }

    public String getUserName() {
        if (roadModel == null) {
            return null;
        }
        return roadModel.getUser().getName();
    }

    public String getSpotCount() {
        if (roadModel.getSpots() == null) {
            return "0";
        }
        return roadModel.getSpots().size() + "";
    }

    public String getAttachmentCount() {
        int imageCount = 0;
        for (List<SpotModel> spotModelList : roadModel.getSpots()) {

            for (SpotModel spotModel : spotModelList) {
                if (spotModel != null && spotModel.getSpotType() == SpotModel.SpotType.IMAGE) {
                    imageCount++;
                }
            }
        }
        return imageCount + "";
    }

    public String getDistance() {
        return BaseApplication.getContext().getString(R.string.distance, roadModel.getDistance());
    }

    public String getDuration() {
        return BaseApplication.getContext().getString(R.string.duration, roadModel.getDuration());
    }

    public String getAddress() {
        return roadModel.getAddress();
    }

    public void onClickViewMap(View view) {
        presenter.startRoadMapActivity(view, roadModel);
    }

    public void onClickShare(View view){
        presenter.startShare(view, roadModel);
    }
}
