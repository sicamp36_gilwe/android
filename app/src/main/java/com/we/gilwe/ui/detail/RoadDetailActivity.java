package com.we.gilwe.ui.detail;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.we.gilwe.R;
import com.we.gilwe.databinding.RoadDetailActivityBinding;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.ui.common.BaseActivity;
import com.we.gilwe.ui.common.Constants;
import com.we.gilwe.ui.login.LoginContract;
import com.we.gilwe.ui.road.RoadCreateActivity;
import com.we.gilwe.ui.road.RoadFollowActivity;
import com.we.gilwe.ui.road.RoadSimpleListActivity;
import com.we.gilwe.utils.UIUtils;

public class RoadDetailActivity extends BaseActivity implements RoadDetailContract.View {

    private RoadDetailActivityBinding binding;
    private RoadDetailContract.Presenter presenter;
    private RoadDetailAdapter adapter;

    private int id;
    private boolean isReverse = false;

    public static Intent createIntent(Context context, int id) {
        Intent intent = new Intent(context, RoadDetailActivity.class);
        intent.putExtra(Constants.EXTRA_ID, id);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.road_detail_activity);

        initToolbar();
        initData();
        initView();

        presenter = new RoadDetailPresenterImpl();
        presenter.attachView(this);

        presenter.loadRoad(id);
    }

    private void initToolbar() {
        setSupportActionBar(binding.toolbar);

        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle(null);
        }
    }

    private void initData() {
        id = getIntent().getIntExtra(Constants.EXTRA_ID, -1);
    }

    private void initView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.startIcon.setOnClickListener(v -> startAnim());
        binding.togetherIcon.setAlpha(0f);
        binding.togetherIcon.setOnClickListener(v -> {
            Intent intent = RoadFollowActivity.createIntent(RoadDetailActivity.this, id);
            startActivity(intent);
            startAnim();
        });
        binding.newIcon.setAlpha(0f);
        binding.newIcon.setOnClickListener(v -> {
            startActivity(new Intent(RoadDetailActivity.this, RoadCreateActivity.class));
            startAnim();
        });

        // Add decoration for dividers between list items
//        binding.recyclerView.addItemDecoration(new SpacesItemDecoration(0, 15.2f));

    }

    private void addStickyHeader() {
        // Add the sticky headers decoration
        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
        binding.recyclerView.addItemDecoration(headersDecor);
    }

    @Override
    public void showRoad(RoadModel roadModel) {
        adapter = new RoadDetailAdapter(roadModel, presenter);
        binding.recyclerView.setAdapter(adapter);

        binding.toolbarTitle.setText(roadModel.getTitle());

        addStickyHeader();
    }

    private void startAnim() {
        PropertyValuesHolder newTranslationProperty;
        PropertyValuesHolder newAlphaProperty;
        PropertyValuesHolder togetherTranslationProperty;
        PropertyValuesHolder togetherAlphaProperty;

        PropertyValuesHolder stargetProperty;
        int width = binding.startIcon.getWidth();
        int height = binding.startIcon.getHeight();
        if (isReverse) {
            stargetProperty = PropertyValuesHolder.ofFloat("rotation", -45, 0);
            newTranslationProperty = PropertyValuesHolder.ofFloat("translationY", -height + UIUtils.dip2px(this, 6), 0);
            newAlphaProperty = PropertyValuesHolder.ofFloat("alpha", 1, 0);
            togetherTranslationProperty = PropertyValuesHolder.ofFloat("translationX", -width, 0);
            togetherAlphaProperty = PropertyValuesHolder.ofFloat("alpha", 1, 0);
        } else {
            stargetProperty = PropertyValuesHolder.ofFloat("rotation", 0, -45);
            newTranslationProperty = PropertyValuesHolder.ofFloat("translationY", 0, -height + UIUtils.dip2px(this, 6));
            newAlphaProperty = PropertyValuesHolder.ofFloat("alpha", 0f, 1f);
            togetherTranslationProperty = PropertyValuesHolder.ofFloat("translationX", 0, -width);
            togetherAlphaProperty = PropertyValuesHolder.ofFloat("alpha", 0f, 1f);
        }

        isReverse = !isReverse;

        ObjectAnimator startAnimator = ObjectAnimator.ofPropertyValuesHolder(binding.startIcon, stargetProperty);
        ObjectAnimator newAnimator = ObjectAnimator.ofPropertyValuesHolder(binding.newIcon, newTranslationProperty, newAlphaProperty);
        ObjectAnimator togetherAnimator = ObjectAnimator.ofPropertyValuesHolder(binding.togetherIcon, togetherTranslationProperty, togetherAlphaProperty);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(300);
        animatorSet.setInterpolator(new FastOutSlowInInterpolator());

        AnimatorSet.Builder builder = animatorSet.play(newAnimator);
        builder.with(togetherAnimator).with(startAnimator);

        animatorSet.start();
    }

}
