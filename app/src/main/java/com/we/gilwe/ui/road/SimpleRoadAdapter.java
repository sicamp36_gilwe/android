package com.we.gilwe.ui.road;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.we.gilwe.databinding.MainSimpleItemLayoutBinding;
import com.we.gilwe.R;
import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.ui.view.recyclerview.SimpleViewHolder;
import com.we.gilwe.utils.Validator;

import java.util.List;

public class SimpleRoadAdapter extends RecyclerView.Adapter<SimpleViewHolder<SimpleRoadModel, MainSimpleItemLayoutBinding>> {
    private List<SimpleRoadModel> simpleRoadModelList;
    private int width = -3;
    private int height = -3;

    public SimpleRoadAdapter(List<SimpleRoadModel> simpleRoadModelList) {
        this.simpleRoadModelList = simpleRoadModelList;
    }

    public void setWidthAndHeight(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public SimpleViewHolder<SimpleRoadModel, MainSimpleItemLayoutBinding> onCreateViewHolder(ViewGroup parent, int viewType) {
        SimpleViewHolder<SimpleRoadModel, MainSimpleItemLayoutBinding> holder = SimpleViewHolder.create(R.layout.main_simple_item_layout, parent);
        View root = holder.getViewDataBinding().getRoot();
        ViewGroup.LayoutParams layoutParams = root.getLayoutParams();

        if (width > -3 && height > -3) {
            layoutParams.width = width;
            layoutParams.height = height;
        }
        root.setLayoutParams(layoutParams);

        return holder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder<SimpleRoadModel, MainSimpleItemLayoutBinding> holder, int position) {
        MainSimpleItemLayoutBinding binding = holder.getViewDataBinding();
        binding.setViewModel(new RoadItemViewModel(simpleRoadModelList.get(position)));
    }

    @Override
    public int getItemCount() {
        return Validator.isEmpty(simpleRoadModelList) ? 0 : simpleRoadModelList.size();
    }
}