package com.we.gilwe.ui.main;

import com.we.gilwe.model.BaseModel;
import com.we.gilwe.model.MainModel;
import com.we.gilwe.ui.common.BasePresenter;
import com.we.gilwe.ui.common.BaseView;

public interface MainContract {

    interface View extends BaseView {
        void showMainList(MainModel mainModel);
    }

    interface Presenter extends BasePresenter<View> {
        void loadRoadList();
    }
}
