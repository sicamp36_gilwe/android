package com.we.gilwe.ui.login;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.we.gilwe.R;
import com.we.gilwe.model.UserModel;
import com.we.gilwe.preferences.SettingsPreferences;
import com.we.gilwe.ui.common.BaseActivity;
import com.we.gilwe.ui.main.MainActivity;

public class SplashActivity extends BaseActivity implements LoginContract.View {

    private LoginButton loginButton;

    private CallbackManager callbackManager;
    private LoginContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        if (isLoggined()) {
            startMainActivity();
            return;
        }
        LoginManager.getInstance().logOut();

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("public_profile");

        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("yoon", "onSuccess(), loginResult : " + loginResult);

                getProfile(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("yoon", "onCancel()");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("yoon", "onError(), e : " + exception);
            }
        });


        presenter = new LoginPresenterImpl();
        presenter.attachView(this);

    }

    private boolean isLoggined() {
        return SettingsPreferences.getInstance(this).getUserModel() != null;
    }

    private void getProfile(final AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                (object, response) -> {
                    // Application code
                    saveUserModel(accessToken, response);
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void saveUserModel(AccessToken accessToken, GraphResponse response) {
        Log.d("yoon", "getProfile" + response.getJSONObject());

        showProgress("가입 중");

        UserModel userModel = new Gson().fromJson(response.getJSONObject().toString(), UserModel.class);
        presenter.signup(accessToken.getToken(), userModel);
    }

    @Override
    public void onSignupSuccess(UserModel userModel) {
        SettingsPreferences preferences = SettingsPreferences.getInstance(SplashActivity.this);
        preferences.setUserModel(userModel);

        dismissProgress();
        startMainActivity();
    }

    @Override
    public void onSignupFailed() {
        new AlertDialog.Builder(this)
                .setTitle("가입 실패")
                .setMessage("회원 인증에 실패 하였습니다.\n잠시 후에 다시 시도해 주세요.")
                .create().show();
    }

    @Override
    public void onLoginSuccess() {
        //nothing to do
    }
}
