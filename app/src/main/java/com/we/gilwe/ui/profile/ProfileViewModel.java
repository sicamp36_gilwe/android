package com.we.gilwe.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.we.gilwe.BaseApplication;
import com.we.gilwe.model.UserModel;
import com.we.gilwe.preferences.SettingsPreferences;
import com.we.gilwe.ui.login.SplashActivity;

public class ProfileViewModel {
    private Context context;
    private UserModel userModel;

    public ProfileViewModel(Context context, UserModel userModel) {
        this.context = context;
        this.userModel = userModel;
    }

    public String getProfileUrl() {
        return userModel.getProfileUrl();
    }

    public String getName() {
        return userModel.getName();
    }

    public String getMyRoadCount() {
        return userModel.getMyRoadCount();
    }

    public String getWalkedRoadCount() {
        return userModel.getWalkedRoadCount();
    }

    public void onClickLogout(View v) {
        SettingsPreferences preferences = SettingsPreferences.getInstance(BaseApplication.getContext());
        preferences.clear();

        Intent intent = new Intent(v.getContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        v.getContext().startActivity(intent);

    }
}
