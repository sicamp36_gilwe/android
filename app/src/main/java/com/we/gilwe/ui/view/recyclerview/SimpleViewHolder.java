package com.we.gilwe.ui.view.recyclerview;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SimpleViewHolder<T, V extends ViewDataBinding> extends RecyclerView.ViewHolder {
    private T mItem;
    private V mViewDataBinding;

    public V getViewDataBinding() {
        return mViewDataBinding;
    }

    public T getItem() {
        return mItem;
    }

    public void setItem(final T item) {
        mItem = item;
    }

    public static <T, V extends ViewDataBinding> SimpleViewHolder<T, V> create(int layoutId, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new SimpleViewHolder<>(view);
    }

    private SimpleViewHolder(final View itemView) {
        super(itemView);
        mViewDataBinding = DataBindingUtil.bind(itemView);
    }
}

