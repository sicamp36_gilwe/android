package com.we.gilwe.ui.road.presenter;

import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.we.gilwe.R;
import com.we.gilwe.api.DataManager;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SpotModel;
import com.we.gilwe.ui.common.BaseActivity;
import com.we.gilwe.ui.road.RoadFollowActivity;
import com.we.gilwe.utils.GoogleMapRouteUtils;
import com.we.gilwe.utils.MapRouteUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by moltak on 2/25/17.
 */

public class RoadActivityPresenter {
    private BaseActivity activity;
    private List<Map<String, String>> spotListMap = new ArrayList<>();
    private SimpleAdapter adapter;

    @Bind(R.id.road_title_text_view) TextView roadTitleTextView;
    @Bind(R.id.road_description_text_view) TextView roadDescriptionTextView;
    @Bind(R.id.spotListView) ListView spotListView;
    private GoogleMap googleMap;
    private int roadId;

    public void onCreate(BaseActivity activity) {
        this.activity = activity;
        ButterKnife.bind(this, activity);

        adapter = new SimpleAdapter(
                activity,
                spotListMap,
                R.layout.road_simple_item_layout,
                new String[]{"distance"},
                new int[]{R.id.distance_text_view});
        spotListView.setAdapter(adapter);
    }

    public void setMap(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    public void fetch(int id) {
        DataManager.getInstance(activity)
                .getRoad(id)
                .subscribe(this::attachRoadData);
    }

    public void onPause() {
    }

    private void attachRoadData(RoadModel roadModel) {
        roadId = roadModel.getId();
        roadTitleTextView.setText(roadModel.getTitle());
        String description = String.format(Locale.KOREAN, "%s km  |  %d분  |  %s",
                roadModel.getDistance(),
                roadModel.getDuration(),
                roadModel.getAddress());
        roadDescriptionTextView.setText(description);

        Observable.from(roadModel.getSpots())
                .map(this::createListChildView)
                .observeOn(AndroidSchedulers.mainThread())
                .toList()
                .subscribe(i -> {
                    spotListMap.addAll(i);
                    adapter.notifyDataSetChanged();
                });
        try {
            GoogleMapRouteUtils.drawRouteFlag(roadModel, googleMap);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Map<String, String> createListChildView(List<SpotModel> spot) {
        Map<String, String> map = new HashMap<>();
        map.put("distance", String.valueOf(spot.get(0).getDistance()));
        return map;
    }

    @OnClick(R.id.looking_for_road_btn)
    public void onButtonClicked(View view) {
        activity.startActivity(RoadFollowActivity.createIntent(activity, roadId));
    }

    @OnItemClick(R.id.spotListView)
    public void onItemSelected(int position) {
        activity.finish();
    }
}
