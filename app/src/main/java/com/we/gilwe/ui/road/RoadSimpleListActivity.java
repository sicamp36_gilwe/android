package com.we.gilwe.ui.road;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.we.gilwe.R;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.ui.common.BaseActivity;
import com.we.gilwe.ui.common.Constants;
import com.we.gilwe.ui.road.presenter.RoadActivityPresenter;

/**
 * Created by moltak on 2/24/17.
 */

public class RoadSimpleListActivity extends BaseActivity implements OnMapReadyCallback {

    RoadActivityPresenter presenter = new RoadActivityPresenter();
    private int roadId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.road_simple_list_activity);

        roadId = getIntent().getIntExtra(Constants.EXTRA_ID, -1);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        presenter.onCreate(this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setMyLocationEnabled(true);
        presenter.setMap(map);
        presenter.fetch(roadId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }
}
