package com.we.gilwe.ui.road.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.ViewGroup;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;
import com.we.gilwe.R;
import com.we.gilwe.databinding.DetailImageItemLayoutBinding;
import com.we.gilwe.databinding.DetailSectionLayoutBinding;
import com.we.gilwe.databinding.DetailTextItemLayoutBinding;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SpotModel;
import com.we.gilwe.ui.detail.RoadDetailContract;
import com.we.gilwe.ui.detail.viewmodel.RoadImageItemViewModel;
import com.we.gilwe.ui.detail.viewmodel.RoadSectionViewModel;
import com.we.gilwe.ui.detail.viewmodel.RoadTextItemViewModel;
import com.we.gilwe.ui.road.RoadFollowActivity;
import com.we.gilwe.ui.view.recyclerview.SimpleViewHolder;
import com.we.gilwe.utils.Validator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;

public class RoadFollowAdapter extends RecyclerView.Adapter<SimpleViewHolder> implements StickyRecyclerHeadersAdapter<SimpleViewHolder> {
    private static final int VIEW_TYPE_IMAGE_ITEM = 2;
    private static final int VIEW_TYPE_TEXT_ITEM = 3;
    private final RoadFollowActivity activity;

    private RoadModel roadModel;
    //    private List<SpotModel> spotModelList = new ArrayList<>();
    private Map<Integer, List<SpotModel>> spotListMap = new HashMap<>();

    private RoadDetailContract.Presenter presenter;

    public RoadFollowAdapter(RoadFollowActivity roadFollowActivity, RoadModel roadModel, RoadDetailContract.Presenter presenter) {
        this.activity = roadFollowActivity;
        this.roadModel = roadModel;
        this.presenter = presenter;
//        this.spotModelList = roadModel.getSpots().get(0);
        makeGroupSpotModel(roadModel.getSpots());
    }

    private void makeGroupSpotModel(List<List<SpotModel>> spotModelList) {
        int index = 0;  //header
        for (int i = 0; i < spotModelList.size(); i++) {
            List<SpotModel> spotModels = spotModelList.get(i);

            try {
                //remove empty attachment spot
                List<SpotModel> result = Observable.from(spotModels)
                        .filter(spotModel -> spotModel != null && spotModel.getSpotType() != SpotModel.SpotType.UNKNOWN)
                        .toList()
                        .filter(Validator::isNotEmpty)
                        .toBlocking().toFuture().get();
                spotListMap.put(index, result);
                index += spotModels.size();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SimpleViewHolder viewHolder = null;
        switch (viewType) {
            case VIEW_TYPE_IMAGE_ITEM:
                viewHolder = SimpleViewHolder.create(R.layout.detail_image_item_layout, parent);
                break;
            case VIEW_TYPE_TEXT_ITEM:
                viewHolder = SimpleViewHolder.create(R.layout.detail_text_item_layout, parent);
                break;

        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        if (holder == null) {
            return;
        }

        int itemViewType = getItemViewType(position);
        switch (itemViewType) {
            case VIEW_TYPE_IMAGE_ITEM:
                DetailImageItemLayoutBinding imageBinding = (DetailImageItemLayoutBinding) holder.getViewDataBinding();
                imageBinding.setViewModel(new RoadImageItemViewModel(getSpotModel(position), presenter));
                break;
            case VIEW_TYPE_TEXT_ITEM:
                DetailTextItemLayoutBinding textBinding = (DetailTextItemLayoutBinding) holder.getViewDataBinding();
                textBinding.setViewModel(new RoadTextItemViewModel(getSpotModel(position), presenter));
                break;
        }
    }

    @Nullable
    private SpotModel getSpotModel(int position) {
        SpotModel spotModel = null;
        for (Integer key : spotListMap.keySet()) {
            List<SpotModel> spotModels = spotListMap.get(key);
            int size = spotModels.size();
            if (position < size) {
                spotModel = spotModels.get(position);
            } else {
                position -= size;
            }
        }
        return spotModel;
    }

    private Integer getKey(int position) {
        for (Integer key : spotListMap.keySet()) {
            List<SpotModel> spotModels = spotListMap.get(key);
            int size = spotModels.size();
            if (position < size) {
                return key;
            } else {
                position -= size;
            }
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (Validator.isEmpty(spotListMap)) {
            return count;
        }

        for (Integer key : spotListMap.keySet()) {
            List<SpotModel> spotModels = spotListMap.get(key);
            count += spotModels.size();
        }

        return count;
    }

    @Override
    public int getItemViewType(int position) {
        SpotModel spotModel = getSpotModel(position);
        if (spotModel == null) {
            return 1010;
        }

        switch (spotModel.getSpotType()) {
            case IMAGE:
                return VIEW_TYPE_IMAGE_ITEM;
            case TEXT:
                return VIEW_TYPE_TEXT_ITEM;
        }

        return -1;
    }

    @Override
    public long getHeaderId(int position) {
        return getKey(position);
//        return -1;
    }

    @Override
    public SimpleViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return SimpleViewHolder.create(R.layout.detail_section_layout, parent);
    }

    @Override
    public void onBindHeaderViewHolder(SimpleViewHolder holder, int position) {

        Integer key = getKey(position);
        List<SpotModel> spotModels = spotListMap.get(key);
        int attachmentCount = 0;
        for (SpotModel spotModel : spotModels) {
            if (spotModel != null && !TextUtils.isEmpty(spotModel.getText()) && !TextUtils.isEmpty(spotModel.getImage())) {
                attachmentCount++;
            }
        }

        DetailSectionLayoutBinding binding = (DetailSectionLayoutBinding) holder.getViewDataBinding();
        binding.setViewModel(new RoadSectionViewModel(attachmentCount, spotModels.get(0), presenter));
        binding.executePendingBindings();
    }
}
