package com.we.gilwe.ui.common;

/**
 * =author Seongbok Yoon Yoon on 16. 6. 27.
 */
public final class Constants {
    public static final int SPAN_COUNT = 2;
    public static final float SEPARATOR_PIXEL = 18.8f;

    private Constants() {
    }

    public static final int REQUEST_CODE_MENU = 1;

    public static final String EXTRA_MENU_MODEL = "EXTRA_MENU_MODEL";

    public static final String EXTRA_DATA = "EXTRA_DATA";

    public static final String EXTRA_TITLE = "EXTRA_TITLE";

    public static final String EXTRA_ITEM_MODEL_LIST = "EXTRA_ITEM_MODEL_LIST";

    public static final String EXTRA_PROFILE_TAB_TYPE = "EXTRA_PROFILE_TAB_TYPE";
    public static final String EXTRA_ID = "EXTRA_ID";
    public static final String EXTRA_ROAD_MODEL = "EXTRA_ROAD_MODEL";
}
