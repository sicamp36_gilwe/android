package com.we.gilwe.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.we.gilwe.BaseApplication;
import com.we.gilwe.R;
import com.we.gilwe.databinding.MainHorizontalListItemLayoutBinding;
import com.we.gilwe.databinding.MainSectionItemLayoutBinding;
import com.we.gilwe.databinding.MainSimpleItemLayoutBinding;
import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.model.MainModel;
import com.we.gilwe.ui.road.RoadItemViewModel;
import com.we.gilwe.ui.view.recyclerview.SimpleViewHolder;
import com.we.gilwe.ui.view.recyclerview.SpacesItemDecoration;
import com.we.gilwe.utils.UIUtils;
import com.we.gilwe.utils.Validator;

import java.util.List;

class MainListAdapter extends RecyclerView.Adapter<SimpleViewHolder> {

    private static final int VIEW_TYPE_SECTION = 1;
    private static final int VIEW_TYPE_ITEM_HORIZONTAL_LIST = 2;
    private static final int VIEW_TYPE_ITEM = 3;

    private MainModel mainModel;

    MainListAdapter(MainModel mainModel) {
        this.mainModel = mainModel;
    }

    void setItem(MainModel mainModel) {
        this.mainModel = mainModel;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SimpleViewHolder holder = null;
        switch (viewType) {
            case VIEW_TYPE_SECTION:
                holder = SimpleViewHolder.create(R.layout.main_section_item_layout, parent);
                break;
            case VIEW_TYPE_ITEM_HORIZONTAL_LIST:
                holder = SimpleViewHolder.create(R.layout.main_horizontal_list_item_layout, parent);
                MainHorizontalListItemLayoutBinding binding = (MainHorizontalListItemLayoutBinding) holder.getViewDataBinding();
                binding.recyclerView.addItemDecoration(new SpacesItemDecoration(10, 0));
                break;
            case VIEW_TYPE_ITEM:
                holder = SimpleViewHolder.create(R.layout.main_simple_item_layout, parent);
                View view = holder.getViewDataBinding().getRoot();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                layoutParams.height = UIUtils.dip2px(view.getContext(), 228);
                int padding = UIUtils.dip2px(view.getContext(), 21);
                int paddingBottom = UIUtils.dip2px(view.getContext(), 13);
                view.setPadding(padding, 0, padding, paddingBottom);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        List<SimpleRoadModel> items = null;
        switch (getItemViewType(position)) {
            case VIEW_TYPE_SECTION:
                String title;
                if (hasRecommend() && position <= 1) {
                    title = BaseApplication.getContext().getString(R.string.main_section_recommend);
                    items = mainModel.getRecommends();
                } else if (hasNews() && position <= 3) {
                    title = BaseApplication.getContext().getString(R.string.main_section_new);
                    items = mainModel.getNews();
                } else {
                    title = BaseApplication.getContext().getString(R.string.main_section_recently);
                    items = mainModel.getSees();
                }
                MainSectionItemLayoutBinding sectionBinding = (MainSectionItemLayoutBinding) holder.getViewDataBinding();
                sectionBinding.setViewModel(new SectionViewModel(title, items));
                break;
            case VIEW_TYPE_ITEM_HORIZONTAL_LIST:
                if (hasRecommend() && position <= 1) {
                    items = mainModel.getRecommends();
                } else if (hasNews() && position <= 3) {
                    items = mainModel.getNews();
                }
                MainHorizontalListItemLayoutBinding horizontalBinding = (MainHorizontalListItemLayoutBinding) holder.getViewDataBinding();
                horizontalBinding.setViewModel(new HorizontalListViewModel(horizontalBinding.recyclerView, items));
                break;
            case VIEW_TYPE_ITEM:
                position -= hasRecommend() ? 2 : 0;
                position -= hasNews() ? 2 : 0;
                position -= 1;

                MainSimpleItemLayoutBinding itemBinding = (MainSimpleItemLayoutBinding) holder.getViewDataBinding();
                itemBinding.setViewModel(new RoadItemViewModel(mainModel.getSees().get(position)));
                break;
        }
    }


    @Override
    public int getItemCount() {
        int count = 0;
        count += Validator.isNotEmpty(mainModel.getRecommends()) ? 2 : 0;
        count += Validator.isNotEmpty(mainModel.getNews()) ? 2 : 0;
        count += Validator.isNotEmpty(mainModel.getSees()) ? mainModel.getSees().size() + 1 : 0;

        return count;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < 5) {
            if (position % 2 == 0) {
                return VIEW_TYPE_SECTION;
            } else {
                return VIEW_TYPE_ITEM_HORIZONTAL_LIST;
            }
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    private boolean hasRecommend() {
        return Validator.isNotEmpty(mainModel.getRecommends());
    }

    private boolean hasNews() {
        return Validator.isNotEmpty(mainModel.getNews());
    }
}
