package com.we.gilwe.ui.profile;

import com.we.gilwe.BaseApplication;
import com.we.gilwe.api.DataManager;
import com.we.gilwe.api.error.ErrorHandler;
import com.we.gilwe.model.SimpleRoadModel;

import java.util.List;

import rx.functions.Action1;

public class MyRoadPresenter implements RoadContract.Presenter {
    private RoadContract.View baseView;
    private final DataManager dataManager;

    public MyRoadPresenter() {
        dataManager = DataManager.getInstance(BaseApplication.getContext());
    }

    @Override
    public void attachView(RoadContract.View baseView) {

        this.baseView = baseView;
    }

    @Override
    public void loadRoadList() {
        dataManager.getMyRoadList()
                .subscribe(new Action1<List<SimpleRoadModel>>() {
                    @Override
                    public void call(List<SimpleRoadModel> roadList) {
                        baseView.showRoadList(roadList);
                    }
                }, new ErrorHandler(null));
    }
}
