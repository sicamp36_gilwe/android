package com.we.gilwe.ui.login;

import android.app.AlertDialog;

import com.we.gilwe.BaseApplication;
import com.we.gilwe.api.DataManager;
import com.we.gilwe.api.error.ErrorHandler;
import com.we.gilwe.model.UserModel;

import rx.functions.Action1;

public class LoginPresenterImpl implements LoginContract.Presenter {

    private final DataManager dataManager;
    private LoginContract.View baseView;

    public LoginPresenterImpl() {
        dataManager = DataManager.getInstance(BaseApplication.getContext());
    }

    @Override
    public void attachView(LoginContract.View baseView) {
        this.baseView = baseView;
    }

    @Override
    public void signup(String token, UserModel userModel) {
        dataManager.signup(token, userModel)
                .subscribe(result -> {

                    if (result != null) {
                        baseView.onSignupSuccess(result);
                    } else {
                        baseView.onSignupFailed();
                    }
                }, new ErrorHandler(null));
    }

    @Override
    public void login(String token, UserModel userModel) {
        dataManager.login(token, userModel)
                .subscribe(result -> {
                    if (result != null) {
                        baseView.onLoginSuccess();
                    }
                });
    }
}
