package com.we.gilwe.ui.main;

import android.databinding.BaseObservable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;

import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.ui.road.SimpleRoadAdapter;
import com.we.gilwe.ui.view.recyclerview.SpacesItemDecoration;

import java.util.List;

public class HorizontalListViewModel extends BaseObservable {
    private RecyclerView recyclerView;
    private List<SimpleRoadModel> simpleRoadModel;

    private final SpacesItemDecoration decoration = new SpacesItemDecoration(4);

    HorizontalListViewModel(RecyclerView recyclerView, List<SimpleRoadModel> simpleRoadModel) {
        this.recyclerView = recyclerView;
        this.simpleRoadModel = simpleRoadModel;

        LinearLayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext(), OrientationHelper.HORIZONTAL, false);
        this.recyclerView.setLayoutManager(layoutManager);
//        this.recyclerView.removeItemDecoration(decoration);
        this.recyclerView.setAdapter(new SimpleRoadAdapter(simpleRoadModel));
    }


}
