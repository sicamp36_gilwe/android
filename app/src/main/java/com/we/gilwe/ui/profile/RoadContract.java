package com.we.gilwe.ui.profile;

import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.ui.common.BasePresenter;
import com.we.gilwe.ui.common.BaseView;

import java.util.List;

public interface RoadContract {

    interface View extends BaseView {
        void showRoadList(List<SimpleRoadModel> roadList);

        void setPresenter(RoadContract.Presenter presenter);
    }

    interface Presenter extends BasePresenter<View> {
        void loadRoadList();
    }
}
