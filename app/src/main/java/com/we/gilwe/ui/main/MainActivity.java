package com.we.gilwe.ui.main;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.we.gilwe.R;
import com.we.gilwe.databinding.MainActivityBinding;
import com.we.gilwe.model.MainModel;
import com.we.gilwe.model.UserModel;
import com.we.gilwe.preferences.SettingsPreferences;
import com.we.gilwe.ui.common.BaseActivity;
import com.we.gilwe.ui.profile.ProfileActivity;
import com.we.gilwe.ui.road.RoadCreateActivity;

public class MainActivity extends BaseActivity implements MainContract.View, SwipeRefreshLayout.OnRefreshListener {

    private MainActivityBinding binding;
    private MainContract.Presenter presenter;
    private MainListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        binding.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.newIcon.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, RoadCreateActivity.class)));
        binding.profileImage.setOnClickListener(v -> startActivity(ProfileActivity.createIntent(MainActivity.this)));

        binding.swipeRefreshLayout.setOnRefreshListener(this);
        presenter = new MainPresenterImpl(this);
        presenter.attachView(this);

        loadRoadList();
    }

    @Override
    public void onRefresh() {
        loadRoadList();
    }

    private void loadRoadList() {
        presenter.loadRoadList();
    }

    @Override
    public void showMainList(MainModel mainModel) {
        binding.swipeRefreshLayout.setRefreshing(false);

        if (binding.recyclerView.getAdapter() == null) {
            adapter = new MainListAdapter(mainModel);
            binding.recyclerView.setAdapter(adapter);
        } else {
            adapter.setItem(mainModel);
            adapter.notifyDataSetChanged();
        }
    }
}
