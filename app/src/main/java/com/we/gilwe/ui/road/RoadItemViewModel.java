package com.we.gilwe.ui.road;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.we.gilwe.BaseApplication;
import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.preferences.SettingsPreferences;
import com.we.gilwe.ui.detail.RoadDetailActivity;
import com.we.gilwe.utils.ToastHelper;

public class RoadItemViewModel extends BaseObservable {
    private SimpleRoadModel simpleRoadModel;

    public RoadItemViewModel(SimpleRoadModel simpleRoadModel) {
        this.simpleRoadModel = simpleRoadModel;
    }

    public String getImageUrl() {
        return simpleRoadModel.getImage();
    }

    public String getTitleText() {
        return simpleRoadModel.getTitle();
    }

    public String getAddress() {
        return simpleRoadModel.getAddress();
    }

    public String getBookmarkCount() {
        return String.valueOf(simpleRoadModel.getBookMarkCount());
    }

    public boolean getSelected() {
        return simpleRoadModel.isBookmarked();
    }

    public void onClickItem(View v) {
        Intent intent = RoadDetailActivity.createIntent(v.getContext(), simpleRoadModel.getId());
        v.getContext().startActivity(intent);
    }

    public void onClickBookMark(View v) {
        //FIXME fix api call
        SettingsPreferences preferences = SettingsPreferences.getInstance(BaseApplication.getContext());

        int count  = simpleRoadModel.getBookMarkCount();

        count = preferences.isBookMarked(simpleRoadModel.getId()) ? Math.max(count - 1, 0) : count + 1;
        simpleRoadModel.setBookMarkCount(count);
        preferences.setBookMark(simpleRoadModel);
        notifyChange();
    }
}
