package com.we.gilwe.ui.login;

import com.we.gilwe.model.UserModel;
import com.we.gilwe.ui.common.BasePresenter;
import com.we.gilwe.ui.common.BaseView;

public interface LoginContract {
    interface View extends BaseView {
        void onSignupSuccess(UserModel result);

        void onLoginSuccess();

        void onSignupFailed();
    }

    interface Presenter extends BasePresenter<View> {
        void signup(String token, UserModel userModel);

        void login(String token, UserModel userModel);
    }
}
