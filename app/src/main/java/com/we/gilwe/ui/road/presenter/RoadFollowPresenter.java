package com.we.gilwe.ui.road.presenter;

import android.content.Intent;
import android.view.View;

import com.we.gilwe.BaseApplication;
import com.we.gilwe.api.DataManager;
import com.we.gilwe.api.error.ErrorHandler;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SpotModel;
import com.we.gilwe.ui.common.Constants;
import com.we.gilwe.ui.detail.RoadDetailContract;
import com.we.gilwe.ui.road.RoadSimpleListActivity;
import com.we.gilwe.utils.ToastHelper;

/**
 * Created by moltak on 2/26/17.
 */

public class RoadFollowPresenter implements RoadDetailContract.Presenter {
    private final DataManager dataManager;
    private RoadDetailContract.View baseView;

    public RoadFollowPresenter() {
        dataManager = DataManager.getInstance(BaseApplication.getContext());
    }

    @Override
    public void attachView(RoadDetailContract.View baseView) {
        this.baseView = baseView;
    }

    @Override
    public void loadRoad(int id) {
        dataManager.getRoad(id)
                .subscribe(roadModel -> {
                    baseView.showRoad(roadModel);
                }, new ErrorHandler(null));
    }

    @Override
    public void startRoadMapActivity(View view, RoadModel roadModel) {
        Intent intent = new Intent(view.getContext(), RoadSimpleListActivity.class);
        intent.putExtra(Constants.EXTRA_ROAD_MODEL, roadModel);
        view.getContext().startActivity(intent);
    }

    @Override
    public void onClickLike(View v, SpotModel spotModel) {
        ToastHelper.show("Like!!!!");
    }

    @Override
    public void onClickBookMark(View v, int roadId) {
        ToastHelper.show("BookMark!!!!");
    }

    @Override
    public void startShare(View view, RoadModel roadModel) {

    }
}
