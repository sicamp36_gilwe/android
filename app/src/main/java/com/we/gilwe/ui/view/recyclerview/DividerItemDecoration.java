package com.we.gilwe.ui.view.recyclerview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.we.gilwe.R;
import com.we.gilwe.utils.UIUtils;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private final Drawable mDivider;
    private final int height;

    public DividerItemDecoration(Context context) {

//        int color = ContextCompat.getColor(context, R.color.color_979797);
        int color = ContextCompat.getColor(context, android.R.color.transparent);
        mDivider = new ColorDrawable(color);

        height = UIUtils.dip2px(context, 1.f);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + height;

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }
}