package com.we.gilwe.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.we.gilwe.R;
import com.we.gilwe.api.DataManager;
import com.we.gilwe.api.error.ErrorHandler;
import com.we.gilwe.model.SpotParam;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomBaseDialog extends Dialog {

    private final Intent intent;
    private int mode, id;
    private double lat, lng;

    @Bind(R.id.spot_description_edit_text)
    EditText spotDescriptionEditText;
    @Bind(R.id.alert_layout)
    View alertLayout;
    @Bind(R.id.spot_alert_edit_text)
    EditText spotAlertEditText;
    private DataManager dataManager;

    public static Dialog createDialog(Context context, Intent i) {
        return new CustomBaseDialog(context, i);
    }


    public CustomBaseDialog(Context context, Intent i) {
        super(context, 0);
        this.intent = i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.activity_custom_base_dialog, null);
        ButterKnife.bind(this, view);

        setContentView(view);

        dataManager = DataManager.getInstance(getContext());

        id = intent.getIntExtra("id", 1);
        mode = intent.getIntExtra("mode", 0);
        lat = intent.getDoubleExtra("lat", 0.0);
        lng = intent.getDoubleExtra("lng", 0.0);

        switch (mode) {
            case 0: // text
                spotDescriptionEditText.setVisibility(View.VISIBLE);
                break;
            case 1: // image
                break;
            case 2: // alert
                alertLayout.setVisibility(View.VISIBLE);
                break;

        }
    }

    @OnClick({
            R.id.spot_create_cancel_btn,
            R.id.spot_create_btn
    })
    public void onButtonClicked(View view) {
        if (view.getId() == R.id.spot_create_cancel_btn) {
            dismiss();
            return;
        }

        switch (mode) {
            case 0: // text
                sendSpot(lat, lng, "", spotDescriptionEditText.getText().toString(), "");
                break;
            case 1: // image
                sendSpot(lat, lng, "", "url", "");
                break;
            case 2: // alert
                sendSpot(lat, lng, "", "", spotAlertEditText.getText().toString());
                break;
        }
        dismiss();
    }

    private void sendSpot(double lat, double lng, String imgUrl, String text, String alert) {
        SpotParam spotParam = new SpotParam(
                id, lat, lng, imgUrl, text, alert);
        dataManager.spot(spotParam)
                .subscribe(i -> {
                    Log.d("API", "result : " + i.getResult());
                }, new ErrorHandler(null));
    }
}
