package com.we.gilwe.ui.detail;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.we.gilwe.BaseApplication;
import com.we.gilwe.api.DataManager;
import com.we.gilwe.api.error.ErrorHandler;
import com.we.gilwe.config.AppDefines;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SpotModel;
import com.we.gilwe.ui.common.Constants;
import com.we.gilwe.ui.road.RoadSimpleListActivity;
import com.we.gilwe.utils.ToastHelper;

class RoadDetailPresenterImpl implements RoadDetailContract.Presenter {

    private final DataManager dataManager;
    private RoadDetailContract.View baseView;

    RoadDetailPresenterImpl() {
        dataManager = DataManager.getInstance(BaseApplication.getContext());
    }

    @Override
    public void attachView(RoadDetailContract.View baseView) {
        this.baseView = baseView;
    }

    @Override
    public void loadRoad(int id) {
        dataManager.getRoad(id)
                .subscribe(roadModel -> {
                    baseView.showRoad(roadModel);
                }, new ErrorHandler(null));
    }

    @Override
    public void startRoadMapActivity(View view, RoadModel roadModel) {
        Intent intent = new Intent(view.getContext(), RoadSimpleListActivity.class);
        intent.putExtra(Constants.EXTRA_ID, roadModel.getId());
        view.getContext().startActivity(intent);
    }

    @Override
    public void onClickLike(View v, SpotModel spotModel) {
        ToastHelper.show("Like!!!!");
    }

    @Override
    public void onClickBookMark(View v, int roadId) {
        ToastHelper.show("BookMark!!!!");
    }

    @Override
    public void startShare(View view, RoadModel roadModel) {
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, roadModel.getTitle());
            intent.putExtra(Intent.EXTRA_TEXT, AppDefines.BASE_SHARE_URL);
            view.getContext().startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
