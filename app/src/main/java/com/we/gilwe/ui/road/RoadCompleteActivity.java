package com.we.gilwe.ui.road;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import com.we.gilwe.R;
import com.we.gilwe.api.DataManager;
import com.we.gilwe.api.error.ErrorHandler;
import com.we.gilwe.databinding.RoadCompleteActivityBinding;
import com.we.gilwe.ui.common.BaseActivity;
import com.we.gilwe.ui.common.Constants;
import com.we.gilwe.ui.detail.RoadDetailActivity;
import com.we.gilwe.utils.ToastHelper;

public class RoadCompleteActivity extends BaseActivity {

    private RoadCompleteActivityBinding binding;
    private DataManager dataManager;
    private int roadId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.road_complete_activity);

        roadId = getIntent().getIntExtra(Constants.EXTRA_ID, -1);

        dataManager = DataManager.getInstance(this);
        binding.completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                complete();
            }
        });

        binding.completeBtn.setEnabled(false);
        binding.titleText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                binding.completeBtn.setEnabled(!TextUtils.isEmpty(s));
            }
        });
    }

    public void complete() {
        dataManager.end(roadId, binding.titleText.getText().toString())
                .subscribe(i -> {
                    onCompleted();
                }, new ErrorHandler(null));
    }

    private void onCompleted() {
        ToastHelper.show("길 등록이 완료 되었습니다.");
        setResult(RESULT_OK);
        finish();

        startActivity(RoadDetailActivity.createIntent(RoadCompleteActivity.this, roadId));
    }


}
