package com.we.gilwe.ui.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.we.gilwe.api.DataManager;
import com.we.gilwe.api.error.ErrorHandler;
import com.we.gilwe.model.BaseModel;
import com.we.gilwe.model.MainModel;

import rx.functions.Action1;

public class MainPresenterImpl implements MainContract.Presenter {

    @Nullable
    private MainContract.View baseView;
    @NonNull
    private final DataManager dataManager;

    public MainPresenterImpl(Context context) {
        dataManager = DataManager.getInstance(context);
    }

    @Override
    public void attachView(MainContract.View baseView) {
        this.baseView = baseView;
    }

    @Override
    public void loadRoadList() {
        dataManager.getHome()
                .subscribe(mainModel -> {
                    if (baseView != null) {
                        baseView.showMainList(mainModel);
                    }
                }, new ErrorHandler(null));

    }
}
