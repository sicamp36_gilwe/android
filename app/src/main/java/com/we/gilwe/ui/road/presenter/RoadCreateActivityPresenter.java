package com.we.gilwe.ui.road.presenter;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.we.gilwe.api.DataManager;
import com.we.gilwe.api.error.ErrorHandler;
import com.we.gilwe.location.LocationController;
import com.we.gilwe.location.LocationEventBus;
import com.we.gilwe.model.BaseModel;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SpotParam;
import com.we.gilwe.ui.common.Constants;
import com.we.gilwe.ui.dialog.CustomBaseDialog;
import com.we.gilwe.ui.road.RoadCompleteActivity;
import com.we.gilwe.ui.road.RoadCreateActivity;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by moltak on 2/25/17.
 */

public class RoadCreateActivityPresenter {
    private RoadCreateActivity activity;
    private LocationController locationController;
    private boolean sending;
    private Subscription subscription, subscription2;
    private DataManager dataManager;
    private LatLng latLng;
    private RoadModel roadModel = new RoadModel();

    public void setActivity(RoadCreateActivity roadCreateActivity) {
        this.activity = roadCreateActivity;
        this.dataManager = DataManager.getInstance(roadCreateActivity);

        subscription = LocationEventBus
                .getInstance()
                .filter(i -> i != null)
                .map(i -> new LatLng(i.getLocation().getLatitude(), i.getLocation().getLongitude()))
                .subscribe(i -> {
                    this.latLng = i;
                }, Throwable::printStackTrace);

        subscription2 = Observable.interval(1, 10, TimeUnit.SECONDS)
                .subscribe(i -> {
                    if (sending) sendSpot(latLng, "", "", "");
                });
    }

    public void setLocationController(LocationController locationController) {
        this.locationController = locationController;
    }

    public void onDestroy() {
        subscription.unsubscribe();
        subscription2.unsubscribe();
    }

    public void startRoad() {
        this.sending = true;

        dataManager.start("오늘 걷기 좋은 날")
                .subscribe(roadModel -> {
                    this.roadModel = roadModel;
                }, new ErrorHandler(null));
    }

    public void pauseRoad() {
        this.sending = false;
    }

    public void stopRoad() {
        this.sending = false;
        Intent intent = new Intent(activity, RoadCompleteActivity.class);
        intent.putExtra(Constants.EXTRA_ID, roadModel.getId());
        activity.startActivityForResult(intent, 1001);
    }

    private void sendSpot(LatLng latLng, String imgUrl, String text, String alert) {
        SpotParam spotParam = new SpotParam(
                this.roadModel.getId(), latLng.latitude, latLng.longitude, imgUrl, text, alert);
        dataManager.spot(spotParam)
                .subscribe(i -> {
                    Log.d("API", "result : " + i.getResult());
                }, new ErrorHandler(null));
    }

    public void createText() {
        showDialog(0);
    }

    public void createImage(String path) {
        sendSpot(latLng, path, "", "");
    }

    public void createAlert() {
        showDialog(2);
    }

    private void showDialog(int mode) {
        Intent i = new Intent(activity, CustomBaseDialog.class);
        i.putExtra("mode", mode);
        i.putExtra("lat", locationController.getLastLocation().getLatitude());
        i.putExtra("lng", locationController.getLastLocation().getLongitude());
        i.putExtra("id", roadModel.getId());

        CustomBaseDialog.createDialog(activity, i).show();
    }
}
