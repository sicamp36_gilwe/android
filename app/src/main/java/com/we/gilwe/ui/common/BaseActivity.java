package com.we.gilwe.ui.common;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.we.gilwe.R;

/**
 * 액티비티 공통 처리 수행
 */
public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, String.format("%s.onCreate()", getClass().getSimpleName()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, String.format("%s.onResume()", getClass().getSimpleName()));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, String.format("%s.onPause()", getClass().getSimpleName()));
        dismissProgress();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, String.format("%s.onDestroy()", getClass().getSimpleName()));
        dismissProgress();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, String.format("%s.onStart()", getClass().getSimpleName()));
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, String.format("%s.onDestroy()", getClass().getSimpleName()));
    }

    public void showProgress(String msg) {
        if (progressDialog != null && progressDialog.isShowing()) {
            dismissProgress();
        }
        progressDialog = ProgressDialog.show(this, "", msg, false, true);
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e(TAG, "requestCode : " + requestCode);
        Log.e(TAG, "resultCode : " + resultCode);
        Log.e(TAG, "data : " + data);
    }

    @BindingAdapter({"app:loadImage"})
    public static void loadImage(ImageView imageView, String url) {
        if (url == null || imageView == null) {
            return;
        }
        Glide.with(imageView.getContext())
                .load(Uri.parse(url))
                .error(R.drawable.default_image)
                .into(imageView);
    }
}
