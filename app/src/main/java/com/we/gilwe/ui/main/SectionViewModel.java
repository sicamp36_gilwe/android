package com.we.gilwe.ui.main;

import android.content.Intent;
import android.view.View;

import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.ui.more.ItemMoreListActivity;

import java.util.List;

public class SectionViewModel {
    private String sectionTitle;
    private List<SimpleRoadModel> simpleRoadModelList;

    SectionViewModel(String sectionTitle, List<SimpleRoadModel> simpleRoadModelList) {

        this.sectionTitle = sectionTitle;
        this.simpleRoadModelList = simpleRoadModelList;
    }

    public String getTitle() {
        return sectionTitle;
    }

    public void onClickMore(View v) {
        Intent intent = ItemMoreListActivity.createIntent(v.getContext(), sectionTitle, simpleRoadModelList);
        v.getContext().startActivity(intent);
    }
}
