package com.we.gilwe.ui.detail.viewmodel;

import com.we.gilwe.model.SpotModel;
import com.we.gilwe.ui.detail.RoadDetailContract;

import java.text.DecimalFormat;
import java.util.Random;

public class RoadSectionViewModel {
    private int attachmentCount;
    private final SpotModel spotModel;
    private final RoadDetailContract.Presenter presenter;

    public RoadSectionViewModel(int attachmentCount, SpotModel spotModel, RoadDetailContract.Presenter presenter) {
        this.attachmentCount = attachmentCount;

        this.spotModel = spotModel;
        this.presenter = presenter;
    }

    public String getDistance() {

        return new DecimalFormat("#.###").format(spotModel.getDistance());
    }

    public String getCommentCount() {
        return new Random().nextInt(1000) + "";
    }

    public String getAttachmentCount() {
        return attachmentCount + "";
    }
}
