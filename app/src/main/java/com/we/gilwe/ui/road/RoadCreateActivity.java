package com.we.gilwe.ui.road;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import com.esafirm.imagepicker.features.camera.DefaultCameraModule;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.we.gilwe.R;
import com.we.gilwe.location.LocationController;
import com.we.gilwe.location.LocationEventBus;
import com.we.gilwe.ui.common.BaseActivity;
import com.we.gilwe.ui.road.presenter.RoadCreateActivityPresenter;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

/**
 * Created by moltak on 2/25/17.
 */

public class RoadCreateActivity extends BaseActivity implements OnMapReadyCallback {
    private LocationController locationController;

    @Bind(R.id.road_create_start_pause_btn)
    CheckBox roadCreateStartPauseBtn;
    RoadCreateActivityPresenter presenter = new RoadCreateActivityPresenter();
    private Subscription subscription;
    private GoogleMap mMap;
    private int RC_REQUEST_CAMERA = 1000;
    private DefaultCameraModule cameraModule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.road_create_activity);
        ButterKnife.bind(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationController = new LocationController(this);
        locationController.locationUpdateStart(true);

        subscription = LocationEventBus.getInstance().subscribe(
                i -> {
                    if (i.getLocation() != null) {
                        double lat = i.getLocation().getLatitude();
                        double lng = i.getLocation().getLongitude();
                        Log.d("location", String.format("%f : %f", lat, lng));
                        if (mMap != null) {
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(lat, lng), 16));
                            mMap.addPolyline(
                                    new PolylineOptions()
                                            .color(0xffe73d25)
                                            .width(13)
                                            .geodesic(true)
                                            .add(new LatLng(lat, lng)));
                        }
                    }
                }
        );

        presenter.setActivity(this);
        presenter.setLocationController(locationController);

        cameraModule = new DefaultCameraModule();
    }

    @Override
    protected void onDestroy() {
        subscription.unsubscribe();
        locationController.locationUpdateStart(false);
        presenter.onDestroy();
        super.onDestroy();
    }

    @OnClick({R.id.road_create_start_pause_btn, R.id.road_create_stop_btn,
            R.id.road_text_create_btn, R.id.road_img_create_btn,
            R.id.road_alert_create_btn})
    public void buttonClick(View view) {
        switch (view.getId()) {
            case R.id.road_create_start_pause_btn:
                if (((CheckBox) view).isChecked()) presenter.startRoad();
                else presenter.pauseRoad();
                break;
            case R.id.road_create_stop_btn:
                presenter.stopRoad();
                break;
            case R.id.road_text_create_btn:
                presenter.createText();
                break;
            case R.id.road_img_create_btn:
                startActivityForResult(cameraModule.getCameraIntent(this), RC_REQUEST_CAMERA);
                break;
            case R.id.road_alert_create_btn:
                presenter.createAlert();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_REQUEST_CAMERA && resultCode == RESULT_OK && data != null) {
            cameraModule.getImage(this, data, images -> {
                presenter.createImage(images.get(0).getPath());
                Log.d("image", images.get(0).getPath());
            });
        }

        if (requestCode == 1001 && resultCode == RESULT_OK) {
            finish();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
    }

}
