package com.we.gilwe.ui.detail;

import android.view.View;

import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SpotModel;
import com.we.gilwe.ui.common.BasePresenter;
import com.we.gilwe.ui.common.BaseView;

public interface RoadDetailContract {
    interface View extends BaseView {
        void showRoad(RoadModel roadModel);
    }

    interface Presenter extends BasePresenter<View> {
        void loadRoad(int id);

        void startRoadMapActivity(android.view.View view, RoadModel roadModel);

        void onClickLike(android.view.View v, SpotModel spotModel);

        void onClickBookMark(android.view.View v, int roadId);

        void startShare(android.view.View view, RoadModel id);
    }
}
