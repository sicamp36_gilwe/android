package com.we.gilwe.ui.detail.viewmodel;

import android.databinding.BaseObservable;
import android.view.View;

import com.we.gilwe.BaseApplication;
import com.we.gilwe.model.SpotModel;
import com.we.gilwe.preferences.SettingsPreferences;
import com.we.gilwe.ui.detail.RoadDetailContract;
import com.we.gilwe.utils.ToastHelper;
import com.we.gilwe.utils.Validator;

public class RoadImageItemViewModel extends BaseObservable {
    private SpotModel spotModel;
    private RoadDetailContract.Presenter presenter;

    public RoadImageItemViewModel(SpotModel spotModel, RoadDetailContract.Presenter presenter) {

        this.spotModel = spotModel;
        this.presenter = presenter;
    }


    public String getText() {
        return spotModel.getText();
    }

    public String getImageUrl() {
        return spotModel.getImage();
    }

    public boolean isLiked() {
        return SettingsPreferences.getInstance(BaseApplication.getContext()).isLiked(spotModel.getId());
    }

    public void onItemClick(View v) {
//        presenter.onClickLike(v, spotModel);

        SettingsPreferences.getInstance(BaseApplication.getContext()).setLike(spotModel.getId());
        notifyChange();
    }
}
