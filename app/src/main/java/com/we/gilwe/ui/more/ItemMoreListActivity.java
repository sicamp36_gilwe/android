package com.we.gilwe.ui.more;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.we.gilwe.R;
import com.we.gilwe.databinding.ItemListActivityBinding;
import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.ui.common.BaseActivity;
import com.we.gilwe.ui.common.Constants;
import com.we.gilwe.ui.road.SimpleRoadAdapter;
import com.we.gilwe.ui.view.recyclerview.SpacesItemDecoration;
import com.we.gilwe.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

public class ItemMoreListActivity extends BaseActivity {

    private String title;
    private List<SimpleRoadModel> simpleRoadModelList;
    private ItemListActivityBinding binding;

    public static Intent createIntent(Context context,
                                      String title,
                                      List<SimpleRoadModel> simpleRoadModelList) {
        Intent intent = new Intent(context, ItemMoreListActivity.class);
        intent.putExtra(Constants.EXTRA_TITLE, title);
        intent.putParcelableArrayListExtra(Constants.EXTRA_ITEM_MODEL_LIST, (ArrayList<? extends Parcelable>) simpleRoadModelList);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.item_list_activity);

        initData();
        initToolbar();

        setAdapter();
    }

    private void setAdapter() {
        int itemSize = (UIUtils.getScreenWidth(this) - UIUtils.dip2px(this, Constants.SEPARATOR_PIXEL) * (Constants.SPAN_COUNT + 1)) / Constants.SPAN_COUNT;

        GridLayoutManager layoutManager = new GridLayoutManager(this, Constants.SPAN_COUNT);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.addItemDecoration(new SpacesItemDecoration(Constants.SEPARATOR_PIXEL));

        SimpleRoadAdapter adapter = new SimpleRoadAdapter(simpleRoadModelList);
        adapter.setWidthAndHeight(itemSize, itemSize);

        binding.recyclerView.setAdapter(adapter);
    }

    private void initToolbar() {
        binding.toolbarTitle.setText(title);
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) {
            return;
        }
        actionBar.setTitle(null);
    }

    private void initData() {
        title = getIntent().getStringExtra(Constants.EXTRA_TITLE);
        simpleRoadModelList = getIntent().getParcelableArrayListExtra(Constants.EXTRA_ITEM_MODEL_LIST);
    }

    public class EqualSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mSpaceHeight;

        public EqualSpaceItemDecoration(int mSpaceHeight) {
            this.mSpaceHeight = mSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = mSpaceHeight;
            outRect.top = mSpaceHeight;
            outRect.left = mSpaceHeight;
            outRect.right = mSpaceHeight;
        }
    }
}
