package com.we.gilwe.ui.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.we.gilwe.R;

public class LoadMoreRecyclerView extends RecyclerView {
    private static final int DEFAULT_THRESHOLD = 5;

    @Nullable
    private OnLoadMoreListener onLoadMoreListener;

    @Nullable
    private LoadMoreRecyclerViewAdapter loadMoreAdapter;

    @NonNull
    private final RecyclerViewDataObserver mObserver = new RecyclerViewDataObserver();

    private boolean isLoadMoreLoading;
    private boolean isLoadMoreFailed;

    @LayoutRes
    private int layoutResId;
    @IdRes
    private int loadingViewId;
    @IdRes
    private int failedViewId;

    private int threshold = DEFAULT_THRESHOLD;

    public LoadMoreRecyclerView(Context context) {
        this(context, null, -1);
    }

    public LoadMoreRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public LoadMoreRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(@Nullable AttributeSet attrs, int defStyle) {

        this.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                shouldNotifyLoadMore();
            }
        });

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.LoadMoreRecyclerView, defStyle, 0);

        layoutResId = typedArray.getResourceId(R.styleable.LoadMoreRecyclerView_loadmore_layout, R.layout.read_more_layout);
        loadingViewId = typedArray.getResourceId(R.styleable.LoadMoreRecyclerView_loadmore_loading_view_id, R.id.footer_loading);
        failedViewId = typedArray.getResourceId(R.styleable.LoadMoreRecyclerView_loadmore_failed_view_id, R.id.footer_retry);
        typedArray.recycle();
    }

    private synchronized void shouldNotifyLoadMore() {
        if (isLoadMoreLoading || isLoadMoreFailed) {
            return;
        }
        if (loadMoreAdapter == null || !loadMoreAdapter.hasMore()) {
            return;
        }
        int lastVisibleItemPosition = findLastVisibleItemPosition();

        int itemCount = getLayoutManager().getItemCount();
        if (itemCount < (lastVisibleItemPosition + threshold)) {
            notifyLoadMore();
        }
    }

    private void notifyLoadMore() {
        isLoadMoreFailed = false;
        isLoadMoreLoading = true;
        if (onLoadMoreListener != null) {
            onLoadMoreListener.onLoadMore();
        }
    }

    private int findLastVisibleItemPosition() {
        LayoutManager layoutManager = getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            LinearLayoutManager manager = (LinearLayoutManager) layoutManager;
            return manager.findLastVisibleItemPosition();

        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            StaggeredGridLayoutManager manager = (StaggeredGridLayoutManager) layoutManager;
            int[] lastVisibleItemPositions = manager.findLastVisibleItemPositions(null);
            int lastPosition = 0;
            for (int lastVisibleItemPosition : lastVisibleItemPositions) {
                lastPosition = Math.max(lastPosition, lastVisibleItemPosition);
            }
            return lastPosition;
        }
        return 0;
    }

    private void notifyLoadMoreViewInvalid() {
        if (loadMoreAdapter == null || !loadMoreAdapter.hasMore()) {
            return;
        }

        loadMoreAdapter.notifyItemChanged(loadMoreAdapter.getItemCount() - 1);
    }

    public void setLayoutResId(@LayoutRes int layoutResId) {
        this.layoutResId = layoutResId;
        notifyLoadMoreViewInvalid();
    }

    public void setLoadingViewId(@IdRes int loadingViewId) {
        this.loadingViewId = loadingViewId;
        notifyLoadMoreViewInvalid();
    }

    public void setFailedViewId(@IdRes int failedViewId) {
        this.failedViewId = failedViewId;
        notifyLoadMoreViewInvalid();
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
        notifyLoadMoreViewInvalid();
    }

    public void onLoadMoreCompleted() {
        isLoadMoreLoading = false;
        notifyLoadMoreViewInvalid();
    }

    public void onLoadMoreFailed() {
        if (loadMoreAdapter != null && loadMoreAdapter.hasMore()) {
            isLoadMoreFailed = true;
            notifyLoadMoreViewInvalid();
        }
    }

    public void setLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    @Override
    public void setAdapter(Adapter adapter) {
        if (getAdapter() != null) {
            getAdapter().unregisterAdapterDataObserver(mObserver);
        }
        loadMoreAdapter = new LoadMoreRecyclerViewAdapter(adapter);

        if (getAdapter() != null) {
            getAdapter().registerAdapterDataObserver(mObserver);
        }
        super.setAdapter(loadMoreAdapter);
    }

    @Override
    public Adapter getAdapter() {
        if (loadMoreAdapter != null) {
            return loadMoreAdapter.getAdapter();
        }
        return null;
    }

    @Override
    public void setLayoutManager(LayoutManager layoutManager) {
        if (layoutManager instanceof GridLayoutManager) {
            final GridLayoutManager manager = (GridLayoutManager) layoutManager;
            final GridLayoutManager.SpanSizeLookup spanSizeLookup = manager.getSpanSizeLookup();

            manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (loadMoreAdapter != null
                            && loadMoreAdapter.hasMore()
                            && position == loadMoreAdapter.getItemCount() - 1) {
                        return manager.getSpanCount();
                    }
                    return spanSizeLookup.getSpanSize(position);
                }
            });
        }
        super.setLayoutManager(layoutManager);
    }

    private class LoadMoreRecyclerViewAdapter extends RecyclerView.Adapter {
        private static final int VIEW_TYPE_LOAD_MORE = Integer.MAX_VALUE - 1;

        @NonNull
        private Adapter adapter;

        LoadMoreRecyclerViewAdapter(@NonNull Adapter adapter) {
            this.adapter = adapter;
        }

        @NonNull
        public Adapter getAdapter() {
            return adapter;
        }

        @Override
        public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_LOAD_MORE:
                    FrameLayout frameLayout = new FrameLayout(parent.getContext());
                    frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    LayoutInflater.from(parent.getContext()).inflate(layoutResId, frameLayout);
                    return new LoadMoreViewHolder(frameLayout);
                default:
                    return adapter.onCreateViewHolder(parent, viewType);
            }
        }

        @SuppressWarnings("all")
        @Override
        public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            switch (holder.getItemViewType()) {
                case VIEW_TYPE_LOAD_MORE:
                    bindLoadMoreViewHolder((LoadMoreViewHolder) holder);
                    break;
                default:
                    adapter.onBindViewHolder(holder, position);
                    break;
            }
        }

        private void bindLoadMoreViewHolder(LoadMoreViewHolder holder) {
            final LoadMoreViewHolder loadMoreViewHolder = holder;
            loadMoreViewHolder.retryView.setVisibility(isLoadMoreFailed ? View.VISIBLE : View.GONE);
            loadMoreViewHolder.loadingView.setVisibility(isLoadMoreFailed ? View.GONE : View.VISIBLE);

            if (holder.itemView.getLayoutParams() instanceof StaggeredGridLayoutManager.LayoutParams) {
                StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
                layoutParams.setFullSpan(true);
            }
            loadMoreViewHolder.retryView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadMoreViewHolder.retryView.setVisibility(View.GONE);
                    loadMoreViewHolder.loadingView.setVisibility(View.VISIBLE);

                    notifyLoadMore();
                }
            });
        }

        @Override
        public final int getItemCount() {
            return hasMore() ? adapter.getItemCount() + 1 : adapter.getItemCount();
        }

        @Override
        public final int getItemViewType(int position) {

            if (hasMore() && isLastPosition(position)) {
                return VIEW_TYPE_LOAD_MORE;
            }
            return adapter.getItemViewType(position);
        }

        @Override
        public final long getItemId(int position) {
            if (hasMore() && isLastPosition(position)) {
                return View.NO_ID;
            }
            return adapter.getItemId(position);
        }

        private boolean isLastPosition(int position) {
            return position == getItemCount() - 1;
        }

        boolean hasMore() {
            if (adapter instanceof LoadMoreInterface) {
                LoadMoreInterface loadMoreInterface = (LoadMoreInterface) this.adapter;
                return loadMoreInterface.hasMore();
            }
            return false;
        }

        private class LoadMoreViewHolder extends RecyclerView.ViewHolder {

            private final View loadingView;
            private final View retryView;

            private LoadMoreViewHolder(View itemView) {
                super(itemView);

                loadingView = itemView.findViewById(loadingViewId);
                retryView = itemView.findViewById(failedViewId);
            }
        }

    }

    private class RecyclerViewDataObserver extends AdapterDataObserver {
        @Override
        public void onChanged() {
            if (loadMoreAdapter != null) {
                loadMoreAdapter.notifyDataSetChanged();
                shouldNotifyLoadMore();
            }
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
            if (loadMoreAdapter != null) {
                loadMoreAdapter.notifyItemRangeChanged(positionStart, itemCount, payload);
            }
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            if (loadMoreAdapter != null) {
                loadMoreAdapter.notifyItemRangeInserted(positionStart, itemCount);
            }
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            if (loadMoreAdapter != null) {
                loadMoreAdapter.notifyItemRangeRemoved(positionStart, itemCount);
            }
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            if (loadMoreAdapter != null) {
                loadMoreAdapter.notifyItemMoved(fromPosition, toPosition);
            }
        }
    }


    public interface LoadMoreInterface {
        boolean hasMore();
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }
}
