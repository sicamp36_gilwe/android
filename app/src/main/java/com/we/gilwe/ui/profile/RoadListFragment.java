package com.we.gilwe.ui.profile;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.we.gilwe.R;
import com.we.gilwe.databinding.SimpleRoadListLayoutBinding;
import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.ui.common.BaseFragment;
import com.we.gilwe.ui.common.Constants;
import com.we.gilwe.ui.road.SimpleRoadAdapter;
import com.we.gilwe.ui.view.recyclerview.SpacesItemDecoration;
import com.we.gilwe.utils.UIUtils;

import java.util.List;

public class RoadListFragment extends BaseFragment implements RoadContract.View {

    private SimpleRoadListLayoutBinding binding;
    private RoadContract.Presenter presenter;
    private int itemSize;

    public static RoadListFragment getInstance(ProfileActivity.TabType tabType) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.EXTRA_PROFILE_TAB_TYPE, tabType);

        RoadListFragment fragment = new RoadListFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public android.view.View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.simple_road_list_layout, container, false);

        binding.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        initRecyclerView();

        loadRoadList();

        return binding.getRoot();
    }

    private void initRecyclerView() {
        itemSize = (UIUtils.getScreenWidth(getActivity()) - UIUtils.dip2px(getActivity(), Constants.SEPARATOR_PIXEL) * (Constants.SPAN_COUNT + 1)) / Constants.SPAN_COUNT;

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), Constants.SPAN_COUNT);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.addItemDecoration(new SpacesItemDecoration(Constants.SEPARATOR_PIXEL));
    }

    private void loadRoadList() {
        presenter.loadRoadList();
    }

    @Override
    public void setPresenter(RoadContract.Presenter presenter) {
        this.presenter = presenter;
        this.presenter.attachView(this);
    }

    @Override
    public void showRoadList(List<SimpleRoadModel> roadList) {

        SimpleRoadAdapter adapter = new SimpleRoadAdapter(roadList);
        adapter.setWidthAndHeight(itemSize, itemSize);

        binding.recyclerView.setAdapter(adapter);
    }
}
