package com.we.gilwe.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBar;
import android.view.View;

import com.we.gilwe.R;
import com.we.gilwe.databinding.ProfileActivityBinding;
import com.we.gilwe.model.UserModel;
import com.we.gilwe.preferences.SettingsPreferences;
import com.we.gilwe.ui.common.BaseActivity;

public class ProfileActivity extends BaseActivity {

    private ProfileActivityBinding binding;

    public static Intent createIntent(Context context) {
        return new Intent(context, ProfileActivity.class);
    }

    public enum TabType {
        MY_ROAD(R.string.create_road),
        WALKED_ROAD(R.string.walked_road),
        BOOKMARK_ROAD(R.string.bookmark);

        private int stringResId;

        TabType(@StringRes int stringResId) {
            this.stringResId = stringResId;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.profile_activity);

        UserModel userModel = SettingsPreferences.getInstance(this).getUserModel();
        binding.setViewModel(new ProfileViewModel(this, userModel));

        binding.viewpager.setAdapter(new ProfilePagerAdapter(getSupportFragmentManager()));
        binding.tabLayout.setupWithViewPager(binding.viewpager);
    }

    private class ProfilePagerAdapter extends FragmentPagerAdapter {
        private TabType[] tabs = TabType.values();

        ProfilePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            RoadContract.Presenter presenter = null;
            switch (tabs[position]) {
                case MY_ROAD:
                    presenter = new MyRoadPresenter();
                    break;
                case WALKED_ROAD:
                    presenter = new WalkedRoadPresenter();
                    break;
                case BOOKMARK_ROAD:
                    presenter = new BookmarkRoadPresenter();
                    break;
            }

            RoadListFragment fragment = RoadListFragment.getInstance(tabs[position]);
            fragment.setPresenter(presenter);

            return fragment;
        }

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(tabs[position].stringResId);
        }
    }
}
