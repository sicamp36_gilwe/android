package com.we.gilwe.location;

import android.location.Location;

/**
 * Created by moltak on 2/24/17.
 */

public class LocationEvent {
    private Location location;

    public LocationEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}