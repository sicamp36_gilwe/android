package com.we.gilwe.location;

import rx.subjects.PublishSubject;

/**
 * Created by moltak on 2/24/17.
 */

public class LocationEventBus {
    private static PublishSubject<LocationEvent> publishSubject = null;

    public static PublishSubject<LocationEvent> getInstance() {
        if(publishSubject == null) {
            synchronized (LocationEventBus.class) {
                publishSubject = PublishSubject.create();
            }
        }

        return publishSubject;
    }
}
