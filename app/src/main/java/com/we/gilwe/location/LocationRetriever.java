package com.we.gilwe.location;

/**
 * Created by moltak on 2/24/17.
 */

public interface LocationRetriever {
    void locationClientConnect();
    void locationClientDisconnect();
    void locationUpdateStart(boolean isService);
    void locationUpdateStop(boolean isService);
    void setLocationParams(int interval, int locationPriority);
}