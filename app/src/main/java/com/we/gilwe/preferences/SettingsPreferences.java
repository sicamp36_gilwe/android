/*
* SettingsPreferences.java
* 복
* 디바이스 토큰 언어설정 저장용 클래스
*/
package com.we.gilwe.preferences;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.model.UserModel;
import com.we.gilwe.utils.StringUtils;

import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class SettingsPreferences extends BasePreferences {

    private static final String PREF = "SettingsPreferences";
    private static SettingsPreferences instance;

    private static final String KEY_DEVICE_ID_KEY = "DEVICE_ID_KEY";
    private static final String KEY_USER_MODEL = "KEY_USER_MODEL";
    private static final String KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN";
    private static final String KEY_BOOK_MARK_ROAD_ID_LIST = "KEY_BOOK_MARK_ROAD_ID_LIST";
    private static final String KEY_LIKE_SPOT_ID_LIST = "KEY_LIKE_SPOT_ID_LIST";

    public static SettingsPreferences getInstance(Context context) {
        if (instance == null) {
            instance = new SettingsPreferences(context, PREF);
        }
        return instance;
    }

    public SettingsPreferences(Context context, String prefsName) {
        super(context, prefsName);
    }

    public String getDeviceId() {
        return (String) get(KEY_DEVICE_ID_KEY, StringUtils.EMPTY_STRING);
    }

    public void initDeviceId(Context context) {
        put(KEY_DEVICE_ID_KEY, getDeviceUUID(context));
    }

    private String getDeviceUUID(Context context) {
        String id = getDeviceId();

        UUID uuid = null;
        if (StringUtils.isNotEmpty(id)) {
            uuid = UUID.fromString(id);
        } else {
            final String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            final String deviceId = null;// = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
            try {
                if (!"9774d56d682e549c".equals(androidId) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    uuid = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
                } else {
                    uuid = deviceId != null ? UUID.nameUUIDFromBytes(deviceId.getBytes("utf8")) : UUID.randomUUID();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        return uuid != null ? uuid.toString() : "";
    }

    public void logout() {
        clear();
    }

    public void setUserModel(UserModel userModel) {
        String json = new Gson().toJson(userModel);
        put(KEY_USER_MODEL, json);
    }

    public UserModel getUserModel() {
        String json = (String) get(KEY_USER_MODEL);

        return new Gson().fromJson(json, UserModel.class);
    }

    public void setBookMark(SimpleRoadModel roadModel) {
        Set<SimpleRoadModel> roadModels = getBookMarkedRoadModels();

        if (roadModels.contains(roadModel)) {
            roadModels.remove(roadModel);
        } else {
            roadModels.add(roadModel);
        }

        put(KEY_BOOK_MARK_ROAD_ID_LIST, new Gson().toJson(roadModels));
    }

    @NonNull
    public Set<SimpleRoadModel> getBookMarkedRoadModels() {
        String json = (String) get(KEY_BOOK_MARK_ROAD_ID_LIST);
        if (TextUtils.isEmpty(json)) {
            return new HashSet<>();
        }

        return new Gson().fromJson(json, new TypeToken<Set<SimpleRoadModel>>() {
        }.getType());
    }

    public boolean isBookMarked(int roadId) {
        Set<SimpleRoadModel> roadModels = getBookMarkedRoadModels();

        for (SimpleRoadModel roadModel : roadModels) {
            if (roadModel != null && roadModel.getId() == roadId) {
                return true;
            }
        }
        return false;
    }


    public void setLike(int spotId) {
        Set<Integer> spotIds = getSpotIds();

        if (spotIds.contains(spotId)) {
            spotIds.remove(spotId);
        } else {
            spotIds.add(spotId);
        }

        put(KEY_LIKE_SPOT_ID_LIST, new Gson().toJson(spotIds));
    }

    @NonNull
    public Set<Integer> getSpotIds() {
        String json = (String) get(KEY_LIKE_SPOT_ID_LIST);
        if (TextUtils.isEmpty(json)) {
            return new HashSet<>();
        }

        return new Gson().fromJson(json, new TypeToken<Set<Integer>>() {
        }.getType());
    }

    public boolean isLiked(int spotId) {
        Set<Integer> likeIds = getSpotIds();
        return likeIds.contains(spotId);
    }
}