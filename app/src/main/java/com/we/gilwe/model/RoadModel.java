package com.we.gilwe.model;

import com.we.gilwe.BaseApplication;
import com.we.gilwe.preferences.SettingsPreferences;

import java.io.Serializable;
import java.util.List;

public class RoadModel implements Serializable {

    /**
     * id : 0
     * text : 사랑할때 걷기 좋은길
     * author : 세일
     * user : {"id":"user1","name":"name","profileUrl":"https://goo.gl/JNdfMt"}
     * address : 서울 역삼 1동
     * distance : 13
     * duration : 90
     * bookMarkCount : 3
     * isBookMarked : true
     * spots : [[{"id":0,"lat":"37.402977000","lng":"127.103962000","distance":"1","time":"2017-02-24T23:00:00Z","address":"주소","image":"https://goo.gl/JNdfMt","favorite":true},{"id":1,"distance":2,"lat":"37.402329000","lng":"127.104284000","time":"2017-02-24T23:03:59Z","text":"text 1","favorite":false}],[{"id":2,"distance":2,"lat":"37.401127000","lng":"127.104907000","time":"2017-02-24T23:11:26Z","image":"https://goo.gl/JNdfMt","favorite":true},{"id":3,"distance":2,"text":"text 2","lat":"37.401127000","lng":"127.104907000","time":"2017-02-24T23:11:26Z","favorite":false}],[{"id":4,"distance":3,"lat":"37.400556000","lng":"127.105218000","image":"https://goo.gl/JNdfMt","time":"2017-02-24T23:15:00Z","favorite":true},{"id":5,"distance":3,"text":"text 3","lat":"37.400556000","lng":"127.105218000","time":"2017-02-24T23:15:00Z","favorite":false}]]
     */

    private int id;
    private String text;
    private String author;
    private String title;
    private UserModel user;
    private String address;
    private float distance;
    private int duration;
    private int bookMarkCount;
    private boolean isBookMarked;
    private String image;
    private List<List<SpotModel>> spots;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getBookMarkCount() {
        if (isBookMarked) {
            return bookMarkCount++;
        }
        return bookMarkCount;
    }

    public void setBookMarkCount(int bookMarkCount) {
        this.bookMarkCount = bookMarkCount;
    }

    public boolean isIsBookMarked() {
        //FIXME
        return SettingsPreferences.getInstance(BaseApplication.getContext()).isBookMarked(id);
    }

    public void setIsBookMarked(boolean isBookMarked) {
        this.isBookMarked = isBookMarked;
    }

    public List<List<SpotModel>> getSpots() {
        return spots;
    }

    public void setSpots(List<List<SpotModel>> spots) {
        this.spots = spots;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
