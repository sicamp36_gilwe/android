package com.we.gilwe.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.we.gilwe.BaseApplication;
import com.we.gilwe.preferences.SettingsPreferences;

public class SimpleRoadModel implements Parcelable {
    /**
     * id : 0
     * title : ‘사랑할때 걷기 좋은 길'
     * image : ‘url’
     * text : 길을 만든 사람이 남긴 글
     */
    private int id;
    private String title;
    private String image;
    private String text;
    private String address;
    private int bookMarkCount;
    private boolean isBookmarked;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getBookMarkCount() {
        if (isBookmarked()) {
            return bookMarkCount++;
        }
        return bookMarkCount;
    }

    public void setBookMarkCount(int bookMarkCount) {
        this.bookMarkCount = bookMarkCount;
    }

    public boolean isBookmarked() {
        //FIXME
        return SettingsPreferences.getInstance(BaseApplication.getContext()).isBookMarked(id);
    }

    public void setBookmarked(boolean bookmarked) {
        isBookmarked = bookmarked;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.image);
        dest.writeString(this.text);
        dest.writeString(this.address);
        dest.writeInt(this.bookMarkCount);
        dest.writeByte(this.isBookmarked ? (byte) 1 : (byte) 0);
    }

    public SimpleRoadModel() {
    }

    protected SimpleRoadModel(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.image = in.readString();
        this.text = in.readString();
        this.address = in.readString();
        this.bookMarkCount = in.readInt();
        this.isBookmarked = in.readByte() != 0;
    }

    public static final Creator<SimpleRoadModel> CREATOR = new Creator<SimpleRoadModel>() {
        @Override
        public SimpleRoadModel createFromParcel(Parcel source) {
            return new SimpleRoadModel(source);
        }

        @Override
        public SimpleRoadModel[] newArray(int size) {
            return new SimpleRoadModel[size];
        }
    };


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleRoadModel that = (SimpleRoadModel) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return id;
    }

    public static SimpleRoadModel create(RoadModel roadModel) {
        SimpleRoadModel simpleRoadModel = new SimpleRoadModel();
        simpleRoadModel.setAddress(roadModel.getAddress());
        simpleRoadModel.setBookmarked(roadModel.isIsBookMarked());
        simpleRoadModel.setImage(roadModel.getImage());
        simpleRoadModel.setText(roadModel.getText());
        simpleRoadModel.setId(roadModel.getId());
        return simpleRoadModel;
    }
}
