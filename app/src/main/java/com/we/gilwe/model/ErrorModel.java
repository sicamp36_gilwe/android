package com.we.gilwe.model;

import java.util.List;

public class ErrorModel {


    /**
     * statusCode : 406
     * name : Customize Exception
     * code : 406
     * message : 전달받은 인자값이 유효하지 않습니다.
     * body : {"isJoi":true,"name":"ValidationError","details":[{"message":"\"phone\" length must be at least 10 characters long","path":"phone","type":"string.min","context":{"limit":10,"value":"123","key":"phone"}}],"_object":{"phone":"123"}}
     */

    private int statusCode;
    private String name;
    private int code;
    private String message;
    /**
     * isJoi : true
     * name : ValidationError
     * details : [{"message":"\"phone\" length must be at least 10 characters long","path":"phone","type":"string.min","context":{"limit":10,"value":"123","key":"phone"}}]
     * _object : {"phone":"123"}
     */

    private BodyModel body;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BodyModel getBody() {
        return body;
    }

    public void setBody(BodyModel body) {
        this.body = body;
    }

    public static class BodyModel {
        private boolean isJoi;
        private String name;
        /**
         * phone : 123
         */

        private ObjectModel _object;
        /**
         * message : "phone" length must be at least 10 characters long
         * path : phone
         * type : string.min
         * context : {"limit":10,"value":"123","key":"phone"}
         */

        private List<DetailsModel> details;

        public boolean isIsJoi() {
            return isJoi;
        }

        public void setIsJoi(boolean isJoi) {
            this.isJoi = isJoi;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ObjectModel get_object() {
            return _object;
        }

        public void set_object(ObjectModel _object) {
            this._object = _object;
        }

        public List<DetailsModel> getDetails() {
            return details;
        }

        public void setDetails(List<DetailsModel> details) {
            this.details = details;
        }

        public static class ObjectModel {
            private String phone;

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            @Override
            public String toString() {
                return "ObjectModel{" +
                        "phone='" + phone + '\'' +
                        '}';
            }
        }

        public static class DetailsModel {
            private String message;
            private String path;
            private String type;
            /**
             * limit : 10
             * value : 123
             * key : phone
             */

            private ContextModel context;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public ContextModel getContext() {
                return context;
            }

            public void setContext(ContextModel context) {
                this.context = context;
            }

            @Override
            public String toString() {
                return "DetailsModel{" +
                        "message='" + message + '\'' +
                        ", path='" + path + '\'' +
                        ", type='" + type + '\'' +
                        ", context=" + context +
                        '}';
            }

            public static class ContextModel {
                private int limit;
                private String value;
                private String key;

                public int getLimit() {
                    return limit;
                }

                public void setLimit(int limit) {
                    this.limit = limit;
                }

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getKey() {
                    return key;
                }

                public void setKey(String key) {
                    this.key = key;
                }

                @Override
                public String toString() {
                    return "ContextModel{" +
                            "limit=" + limit +
                            ", value='" + value + '\'' +
                            ", key='" + key + '\'' +
                            '}';
                }
            }
        }

        @Override
        public String toString() {
            return "BodyModel{" +
                    "isJoi=" + isJoi +
                    ", name='" + name + '\'' +
                    ", _object=" + _object +
                    ", details=" + details +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ErrorModel{" +
                "statusCode=" + statusCode +
                ", name='" + name + '\'' +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", body=" + body +
                '}';
    }
}
