package com.we.gilwe.model;

import android.text.TextUtils;

import java.io.Serializable;

public class SpotModel implements Serializable {


    public enum SpotType {
        IMAGE,
        TEXT,
        UNKNOWN
    }

    /**
     * id : 0
     * lat : 37.402977000
     * lng : 127.103962000
     * distance : 1
     * time : 2017-02-24T23:00:00Z
     * address : 주소
     * image : https://goo.gl/JNdfMt
     * favorite : true
     * text : text 1
     */

    private int id;
    private String lat;
    private String lng;
    private float distance;
    private String time;
    private String address;
    private String image;
    private boolean favorite;
    private String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public SpotType getSpotType() {
        if (!TextUtils.isEmpty(image)) {
            return SpotType.IMAGE;
        }

        if (!TextUtils.isEmpty(text)) {
            return SpotType.TEXT;
        }
        return SpotType.UNKNOWN;
    }
}