package com.we.gilwe.model;

/**
 * Created by moltak on 2/24/17.
 */

public class SpotParam {
    private final int road_id;
    private double lat, lng;
    private String imgUrl, text, alert;

    public SpotParam(int road_id, double lat, double lng, String imgUrl, String text, String alert) {
        this.road_id = road_id;
        this.lat = lat;
        this.lng = lng;
        this.imgUrl = imgUrl;
        this.text = text;
        this.alert = alert;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getText() {
        return text;
    }

    public String getAlert() {
        return alert;
    }

    public int getroad_id() {
        return road_id;
    }
}
