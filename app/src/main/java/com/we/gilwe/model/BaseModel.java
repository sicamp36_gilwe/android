package com.we.gilwe.model;

/**
 * 서버 리턴값 매핑용 공통 슈퍼 클래스
 */
public class BaseModel {
    private String errorCode, result;
    private int code;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getResult() {
        return result;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "BaseModel{" +
                "errorCode='" + errorCode + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
