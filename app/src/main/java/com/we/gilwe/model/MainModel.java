package com.we.gilwe.model;

import java.util.List;

public class MainModel {
    private List<SimpleRoadModel> recommends;
    private List<SimpleRoadModel> news;
    private List<SimpleRoadModel> sees;

    public List<SimpleRoadModel> getRecommends() {
        return recommends;
    }

    public void setRecommends(List<SimpleRoadModel> recommends) {
        this.recommends = recommends;
    }

    public List<SimpleRoadModel> getNews() {
        return news;
    }

    public void setNews(List<SimpleRoadModel> news) {
        this.news = news;
    }

    public List<SimpleRoadModel> getSees() {
        return sees;
    }

    public void setSees(List<SimpleRoadModel> sees) {
        this.sees = sees;
    }
}
