package com.we.gilwe.model;

public class UserResponseModel extends BaseModel {
    private UserModel user;

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }
}
