package com.we.gilwe.model;

import java.io.Serializable;

public class UserModel implements Serializable {
    private String id;
    private String name;
    private String profileUrl;
    private String accessToken;
    private int myRoadCount;
    private int walkedRoadCount;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * picture : {"data":{"is_silhouette":false,"url":"https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/14183701_1138208212906715_550466776317320049_n.jpg?oh=cb504f3a203609426b008fffe7bf5e5f&oe=592EA3AF"}}
     */


    private PictureModel picture;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMyRoadCount() {
        return String.valueOf(myRoadCount);
    }

    public void setMyRoadCount(int myRoadCount) {
        this.myRoadCount = myRoadCount;
    }

    public String getWalkedRoadCount() {
        return String.valueOf(walkedRoadCount);
    }

    public void setWalkedRoadCount(int walkedRoadCount) {
        this.walkedRoadCount = walkedRoadCount;
    }

    public String getProfileUrl() {
        if (picture != null && picture.getData() != null) {
            return picture.getData().getUrl();
        }
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public PictureModel getPicture() {
        return picture;
    }

    public void setPicture(PictureModel picture) {
        this.picture = picture;
    }


    public static class PictureModel implements Serializable {
        /**
         * data : {"is_silhouette":false,"url":"https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/14183701_1138208212906715_550466776317320049_n.jpg?oh=cb504f3a203609426b008fffe7bf5e5f&oe=592EA3AF"}
         */

        private DataModel data;

        public DataModel getData() {
            return data;
        }

        public void setData(DataModel data) {
            this.data = data;
        }

        public static class DataModel implements Serializable {
            /**
             * is_silhouette : false
             * url : https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/14183701_1138208212906715_550466776317320049_n.jpg?oh=cb504f3a203609426b008fffe7bf5e5f&oe=592EA3AF
             */

            private boolean is_silhouette;
            private String url;

            public boolean isIs_silhouette() {
                return is_silhouette;
            }

            public void setIs_silhouette(boolean is_silhouette) {
                this.is_silhouette = is_silhouette;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }


        }
    }

}
