package com.we.gilwe.model;

/**
 * Created by moltak on 2/26/17.
 */

public class CreateRoadParam {
    Integer id;
    String title;

    public CreateRoadParam(Integer id) {
        this.id = id;
    }

    public CreateRoadParam(String title) {
        this.title = title;
    }

    public CreateRoadParam(Integer id, String title) {
        this.id = id;
        this.title = title;
    }
}
