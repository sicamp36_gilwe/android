package com.we.gilwe;

import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.we.gilwe.model.UserModel;
import com.we.gilwe.preferences.SettingsPreferences;

import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by 복 on 16. 6. 14.
 * 앱 초기 설정 메인 클래스
 */
public class BaseApplication extends Application {
    private Scheduler defaultSubscribeScheduler;

    private static Context baseContext;

    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize(getApplicationContext());

        SettingsPreferences.getInstance(this).initDeviceId(this);
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);

        baseContext = this;
    }

    public static BaseApplication get(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }

    //MemberModel to change scheduler from tests
    public void setDefaultSubscribeScheduler(Scheduler scheduler) {
        this.defaultSubscribeScheduler = scheduler;
    }

    public static Context getContext() {
        return baseContext;
    }

}
