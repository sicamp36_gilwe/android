package com.we.gilwe.api.error;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;

import com.we.gilwe.R;
import com.we.gilwe.model.ErrorModel;
import com.we.gilwe.ui.common.BaseActivity;
import com.we.gilwe.utils.ToastHelper;

import rx.functions.Action1;

/**
 * 공통 에러 처리 핸들러
 */
public class ErrorHandler implements Action1<Throwable> {
    private Context context;
    private final View.OnClickListener positiveListener;
    private final View.OnClickListener negativeListener;

    public ErrorHandler(Context context) {
        this(context, null, null);
    }

    public ErrorHandler(Context context, View.OnClickListener positiveListener, View.OnClickListener negativeListener) {
        this.context = context;
        this.positiveListener = positiveListener;
        this.negativeListener = negativeListener;
    }

    @Override
    public void call(Throwable throwable) {
        if (context instanceof BaseActivity) {
            ((BaseActivity) context).dismissProgress();

        }
        if (throwable instanceof RetrofitException) {
            RetrofitException exception = (RetrofitException) throwable;

            switch (exception.getKind()) {
                case HTTP:
                    handleApiError(exception.getErrorModel());
                    break;
                case NETWORK:
                    handleNetworkError(exception);
                    break;
                case UNEXPECTED:
                    ToastHelper.show(exception.getMessage());
                    break;
            }
        } else {
            ToastHelper.show("알 수 없는 오류가 발생 했습니다.\n 잠시후 다시 시도해주세요.");
        }
    }

    private void handleApiError(ErrorModel errorModel) {
        if (errorModel == null) {
            return;
        }
        ToastHelper.show(errorModel.getMessage());
        //TODO handle error
    }

    private void handleNetworkError(RetrofitException exception) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.dialog_network_error_title)
                .setMessage(R.string.dialog_network_error_message)
                .setPositiveButton(R.string.dialog_ok_btn, null)
                .create().show();
    }
}
