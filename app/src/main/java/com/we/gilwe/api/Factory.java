package com.we.gilwe.api;

import android.os.Build;
import android.support.annotation.NonNull;

import com.facebook.AccessToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.we.gilwe.BaseApplication;
import com.we.gilwe.api.error.RxErrorHandlingCallAdapterFactory;
import com.we.gilwe.model.UserModel;
import com.we.gilwe.preferences.SettingsPreferences;

import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.Platform;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class Factory<T> {
    private static final int CONNECT_TIMEOUT = 15;
    private static final int WRITE_TIMEOUT = 15;
    private static final int READ_TIMEOUT = 15;


    private Factory() {
    }

    public static <T> T create(Class<T> clazz, String baseApiUrl) {

        Retrofit retrofit = getRetrofit(baseApiUrl);

        return retrofit.create(clazz);
    }

    @NonNull
    public static Retrofit getRetrofit(String baseApiUrl) {
        return new Retrofit.Builder()
                .baseUrl(HttpUrl.parse(baseApiUrl))
                .addConverterFactory(GsonConverterFactory.create(getGson()))
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .client(makeHttpClient())
                .build();
    }

    @NonNull
    private static Gson getGson() {
        return new GsonBuilder()
                .setLenient()
                .create();
    }

    private static OkHttpClient makeHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        configureClient(httpClient);
        httpClient.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS) //연결 타임아웃 시간 설정
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS) //쓰기 타임아웃 시간 설정
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS) //읽기 타임아웃 시간 설정
                .addInterceptor(getHttpLoggingInterceptor())
                .addInterceptor(getHeaderInterceptor());

        return httpClient.build();
    }

    @NonNull
    private static HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @NonNull
    private static Interceptor getHeaderInterceptor() {
        return new Interceptor() {

            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder newRequest = original.newBuilder();

                UserModel userModel = SettingsPreferences.getInstance(BaseApplication.getContext()).getUserModel();
                String token = null;
                if (userModel != null) {
                    token = userModel.getAccessToken();
                }
                newRequest
                        .method(original.method(), original.body())
//                        .addHeader("User-Agent", getDefaultUserAgent())
                        .addHeader("accessToken", token == null ? "" : token)
                        .addHeader("keyType", "facebook")
//                        .addHeader("accessToken", "changhwaoh_herschel")
//                        .addHeader("keyType", "kakao")
                        .addHeader("Content-Type", "application/json; charset=UTF-8")
                        .addHeader("Accept", "application/json");

                return chain.proceed(newRequest.build());
            }
        };
    }

    private static String getDefaultUserAgent() {
        StringBuilder result = new StringBuilder(64);
        result.append("Dalvik/");
        result.append(System.getProperty("java.vm.version")); // such as 1.1.0
        result.append(" (Linux; U; Android ");

        String version = Build.VERSION.RELEASE; // "1.0" or "3.4b5"
        result.append(version.length() > 0 ? version : "1.0");

        // add the model for the release build
        if ("REL".equals(Build.VERSION.CODENAME)) {
            String model = Build.MODEL;
            if (model.length() > 0) {
                result.append("; ");
                result.append(model);
            }
        }
        String id = Build.ID; // "MASTER" or "M4-rc20"
        if (id.length() > 0) {
            result.append(" Build/");
            result.append(id);
        }
        result.append(")");
        return result.toString();
    }

    /**
     * https handshake 에러 해결.
     * 인증서를 무시하고 통과 되도록 변경하였다.
     *
     * @param builder
     * @return
     */
    public static OkHttpClient.Builder configureClient(final OkHttpClient.Builder builder) {
        final TrustManager[] certs = new TrustManager[]{new X509TrustManager() {

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkServerTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
            }

            @Override
            public void checkClientTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
            }
        }};

        SSLContext ctx = null;
        try {
            ctx = SSLContext.getInstance("TLS");
            ctx.init(null, certs, new SecureRandom());
        } catch (final java.security.GeneralSecurityException ex) {
        }

        try {
            final HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(final String hostname, final SSLSession session) {
                    return true;
                }
            };
//            builder.sslSocketFactory().hostnameVerifier(hostnameVerifier);
            assert ctx != null;
            javax.net.ssl.SSLSocketFactory socketFactory = ctx.getSocketFactory();
            X509TrustManager trustManager = Platform.get().trustManager(socketFactory);
            builder.sslSocketFactory(socketFactory, trustManager);
        } catch (final Exception e) {
        }

        return builder;
    }

}