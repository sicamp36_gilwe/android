package com.we.gilwe.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.we.gilwe.BaseApplication;
import com.we.gilwe.config.AppDefines;
import com.we.gilwe.model.BaseModel;
import com.we.gilwe.model.CreateRoadParam;
import com.we.gilwe.model.MainModel;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.model.SpotParam;
import com.we.gilwe.model.UserModel;
import com.we.gilwe.model.UserResponseModel;
import com.we.gilwe.preferences.SettingsPreferences;
import com.we.gilwe.utils.MultipartUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.http.GET;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;

/**
 * 데이터 불러오기 (from SharedPreference, DataBase and API Server) 및 데이터 가공
 */
public class DataManager {

    private static final String TAG = DataManager.class.getSimpleName();

    private static volatile DataManager mInstance;

    private final BaseApplication application;
    private final ApiService apiService;
    private final GetRoad0 testService;

    public static DataManager getInstance(Context context) {
        if (mInstance == null) {
            synchronized (DataManager.class) {
                if (mInstance == null) {
                    mInstance = new DataManager(context.getApplicationContext());
                }
            }
        }
        return mInstance;
    }

    private DataManager(Context context) {
        apiService = Factory.create(ApiService.class, AppDefines.BASE_URL);
        testService = Factory.create(GetRoad0.class, "https://raw.githubusercontent.com/moltak/gilwe_sample_data/master/sample_data/");
        application = BaseApplication.get(context);
    }

    private interface GetRoad0 {
        @GET("road_0.json")
        Observable<RoadModel> get();

        @GET("card_list.json")
        Observable<List<SimpleRoadModel>> getRoadList();

        @GET("main.json")
        Observable<MainModel> getHome();
    }

    public Observable<UserModel> signup(String token, UserModel userModel) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("accessToken", token);
        jsonObject.addProperty("id", userModel.getId());
        jsonObject.addProperty("name", userModel.getName());
        jsonObject.addProperty("keyType", "facebook");
        jsonObject.addProperty("profileUrl", userModel.getProfileUrl());

        return apiService.singup(jsonObject)
                .map(UserResponseModel::getUser)
                .subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<UserModel> login(String token, UserModel userModel) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("accessToken", token);
        jsonObject.addProperty("id", userModel.getId());
        jsonObject.addProperty("name", userModel.getName());
        jsonObject.addProperty("profileUrl", userModel.getProfileUrl());

        return apiService.login(jsonObject)
                .map(UserResponseModel::getUser)
                .observeOn(application.defaultSubscribeScheduler())
                .subscribeOn(AndroidSchedulers.mainThread());
    }

    public Observable<MainModel> getHome() {
        return apiService.getHome()
                .subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<RoadModel> getRoad(int id) {
        return apiService.getRoad(id)
                .subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
//        return createTempRoad();
    }

    public Observable<List<SimpleRoadModel>> getMyRoadList() {
        UserModel userModel = SettingsPreferences.getInstance(BaseApplication.getContext()).getUserModel();
        String userId = null;
        if (userModel != null) {
            userId = userModel.getId();
        }
        return apiService.getMyRoads(userId)
                .subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<SimpleRoadModel>> getWalkedRoadList() {
        return Observable.empty();
//        return testService.getRoadList()
//                .subscribeOn(application.defaultSubscribeScheduler())
//                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<SimpleRoadModel>> getBookmarkRoadList() {
        return Observable.create((Observable.OnSubscribe<List<SimpleRoadModel>>) subscriber -> {
            SettingsPreferences preferences = SettingsPreferences.getInstance(BaseApplication.getContext());
            subscriber.onNext(new ArrayList<>(preferences.getBookMarkedRoadModels()));
            subscriber.onCompleted();
        }).subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
//        return testService.getRoadList()
//                .subscribeOn(application.defaultSubscribeScheduler())
//                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<RoadModel> start(String title) {
        return apiService.start(new CreateRoadParam(title))
                .subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<BaseModel> end(int id, String title) {
        return apiService.end(new CreateRoadParam(id, title))
                .subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<BaseModel> spot(SpotParam spotParam) {
        return apiService.spot(
                MultipartUtils.createPartFromString(String.valueOf(spotParam.getroad_id())),
                MultipartUtils.createPartFromString(String.valueOf(spotParam.getLat())),
                MultipartUtils.createPartFromString(String.valueOf(spotParam.getLng())),
                MultipartUtils.createPartFromString(spotParam.getText()),
                MultipartUtils.createPartFromString(spotParam.getAlert()),
                MultipartUtils.createImagePart(spotParam.getImgUrl()))
                .subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<BaseModel> spotComment(Integer spotId, String comment) {
        return apiService.spotComment(spotId, comment)
                .subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<BaseModel> spotEmotion(Integer spotId, String emotion) {
        return apiService.spotEmotion(spotId, emotion)
                .subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<BaseModel> spotImage(Integer spotId, String imgPath) {
        return apiService.spotImage(
                spotId,
                MultipartUtils.createImagePart(imgPath))
                .subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public Observable<RoadModel> createTempRoad() {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                String json = null;
                try {
                    InputStream is = application.getAssets().open("road.json");
                    int size = is.available();
                    byte[] buffer = new byte[size];
                    is.read(buffer);
                    is.close();
                    json = new String(buffer, "UTF-8");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                subscriber.onNext(json);
                subscriber.onCompleted();
            }
        })
                .map(new Func1<String, RoadModel>() {
                    @Override
                    public RoadModel call(String s) {
                        return new Gson().fromJson(s, RoadModel.class);
                    }
                }).subscribeOn(application.defaultSubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
