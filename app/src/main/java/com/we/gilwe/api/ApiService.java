package com.we.gilwe.api;

import com.google.gson.JsonObject;
import com.we.gilwe.model.BaseModel;
import com.we.gilwe.model.CreateRoadParam;
import com.we.gilwe.model.MainModel;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SimpleRoadModel;
import com.we.gilwe.model.SpotParam;
import com.we.gilwe.model.UserModel;
import com.we.gilwe.model.UserResponseModel;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * 서버 API 파라미터 매퍼
 */
public interface ApiService {

    @POST("users")
    Observable<UserResponseModel> singup(@Body JsonObject body);

    @POST("login")
    Observable<UserResponseModel> login(@Body JsonObject jsonObject);

    @GET("users/{userId}/roads")
    Observable<List<SimpleRoadModel>> getMyRoads(@Path("userId") String userId);

    @GET("roads")
    Observable<MainModel> getHome();

    @GET("roads/{id}")
    Observable<RoadModel> getRoad(@Path("id") int id);

    @POST("roads/start")
    Observable<RoadModel> start(@Body CreateRoadParam param);

    @POST("roads/end")
    Observable<BaseModel> end(@Body CreateRoadParam param);

    @Multipart
    @POST("spots")
    Observable<BaseModel> spot(
            @Part("road_id") RequestBody road_id,
            @Part("lat") RequestBody lat,
            @Part("lng") RequestBody lng,
            @Part("text") RequestBody text,
            @Part("alert") RequestBody alert,
            @Part MultipartBody.Part image);

    @POST("spot/{id}/comment")
    Observable<BaseModel> spotComment(@Path("id") Integer spotId, @Body String comment);

    @POST("spot/{id}/emotion")
    Observable<BaseModel> spotEmotion(@Path("id") Integer spotId, @Body String emotion);

    @Multipart
    @POST("spot/{id}/image")
    Observable<BaseModel> spotImage(@Path("id") Integer spotId, @Part MultipartBody.Part img);
}
