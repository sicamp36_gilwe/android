package com.we.gilwe.utils;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.we.gilwe.R;
import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SpotModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by moltak on 2/25/17.
 */

public class GoogleMapRouteUtils {
    public static void drawRouteFlag(RoadModel roadModel, GoogleMap map) throws ExecutionException, InterruptedException {
        Observable.from(roadModel.getSpots())
                .map(i -> i.get(0))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(i -> drawFlags(i, map));

        List<SpotModel> routes = new ArrayList<>();
        Observable.from(roadModel.getSpots())
                .map(routes::addAll)
                .toList()
                .subscribe(i -> drawRoutes(routes, map));
    }

    private static void drawFlags(SpotModel spotModel, GoogleMap map) {
        map.addMarker(new MarkerOptions()
                .position(getLatLng(spotModel))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_icon_pin2)));
    }

    private static void drawRoutes(List<SpotModel> routes, GoogleMap map) {
        Observable.from(routes)
                .map(GoogleMapRouteUtils::getLatLng)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(i -> {
                    map.addPolyline(new PolylineOptions().color(0xffe73d25).width(13).geodesic(true).addAll(i));
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(i.get(0), 16));
                });
    }

    @NonNull
    private static LatLng getLatLng(SpotModel i) {
        return new LatLng(
                Double.valueOf(i.getLat()),
                Double.valueOf(i.getLng()));
    }
}
