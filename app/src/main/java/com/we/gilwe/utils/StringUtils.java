package com.we.gilwe.utils;

import java.util.Collection;

public final class StringUtils {
    public static final String EMPTY_STRING = "";

    private StringUtils() {
    }

    public static boolean isBlank(CharSequence s) {
        if (isEmpty(s)) {
            return true;
        }

        final int len = s.length();
        char ch;
        for (int i = 0; i < len; ++i) {
            ch = s.charAt(i);
            switch (ch) {
                case ' ':
                case '\t':
                case '\n':
                case '\r':
                    break;
                default:
                    return false;
            }
        }
        return true;
    }

    public static boolean isNotBlank(CharSequence s) {
        return !isBlank(s);
    }

    public static boolean isEmpty(CharSequence s) {
        return (s == null) ? true : (s.length() == 0);
    }

    public static boolean isNotEmpty(CharSequence s) {
        return !isEmpty(s);
    }

    public static boolean isNumeric(String s) {
        if (isEmpty(s)) {
            return false;
        }

        final int len = s.length();
        char ch;
        for (int i = 0; i < len; ++i) {
            ch = s.charAt(i);
            switch (ch) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    break;
                default:
                    return false;
            }
        }
        return true;
    }

    public static String join(Collection<?> collection, String separator) {
        if (collection == null) {
            return null;
        }
        if (collection.isEmpty()) {
            return EMPTY_STRING;
        }

        if (separator == null) {
            separator = EMPTY_STRING;
        }

        StringBuilder sb = new StringBuilder(collection.size() * 16);
        boolean first = true;
        for (Object obj : collection) {
            if (first) {
                first = false;
            } else {
                sb.append(separator);
            }
            if (obj != null) {
                sb.append(obj);
            }
        }
        return sb.toString();
    }

    public static String trim(String s) {
        return (s == null) ? null : s.trim();
    }

    public static String trimToEmpty(String s) {
        String trimmed = trim(s);
        return (trimmed == null) ? EMPTY_STRING : trimmed;
    }

    public static String trimToNull(String s) {
        String trimmed = trim(s);
        return isEmpty(trimmed) ? null : s;
    }

    public static String replaceEnter(String s) {
        if (StringUtils.isEmpty(s)) {
            return "";
        }
        final int len = s.length();
        char ch;
        StringBuffer strBuffer = new StringBuffer();
        for (int i = 0; i < len; ++i) {
            ch = s.charAt(i);
            if (ch == '\n') {
                ch = ' ';
            }
            strBuffer.append(ch);
        }
        String str = StringUtils.trim(strBuffer.toString());
        str = str.replaceAll("\\s+", " ");
        return str;
    }

}
