package com.we.gilwe.utils;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Log;

import com.we.gilwe.model.RoadModel;
import com.we.gilwe.model.SpotModel;

import net.daum.mf.map.api.CameraUpdateFactory;
import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapPointBounds;
import net.daum.mf.map.api.MapPolyline;
import net.daum.mf.map.api.MapView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by moltak on 2/25/17.
 */

public class MapRouteUtils {
    public static void drawRouteFlag(RoadModel roadModel, MapView mapView) throws ExecutionException, InterruptedException {
        Observable.from(roadModel.getSpots())
                .map(i -> i.get(0))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(i -> drawFlags(i, mapView));

        List<SpotModel> routes = new ArrayList<>();
        Observable.from(roadModel.getSpots())
                .map(routes::addAll)
                .toList()
                .subscribe(i -> drawRoutes(routes, mapView));
    }

    private static void drawFlags(SpotModel spotModel, MapView mapView) {
        MapPOIItem marker = new MapPOIItem();
        marker.setItemName(spotModel.getText());
        marker.setTag(1);
        marker.setMapPoint(getMapPoint(spotModel));
        marker.setMarkerType(MapPOIItem.MarkerType.BluePin);
        marker.setSelectedMarkerType(MapPOIItem.MarkerType.RedPin);

        mapView.addPOIItem(marker);
    }

    private static void drawRoutes(List<SpotModel> routes, MapView mapView) {
        MapPolyline polyline = new MapPolyline();
        polyline.setTag(1000);
        polyline.setLineColor(Color.argb(128, 255, 51, 0));

        Observable.from(routes)
                .map(MapRouteUtils::getMapPoint)
                .subscribe(polyline::addPoint);
        mapView.addPolyline(polyline);

        // 지도뷰의 중심좌표와 줌레벨을 Polyline이 모두 나오도록 조정.g
        MapPointBounds mapPointBounds = new MapPointBounds(polyline.getMapPoints());
        int padding = 100; // px
        mapView.moveCamera(CameraUpdateFactory.newMapPointBounds(mapPointBounds, padding));
    }

    @NonNull
    private static MapPoint getMapPoint(SpotModel i) {
        return MapPoint.mapPointWithGeoCoord(
                Double.valueOf(i.getLat()),
                Double.valueOf(i.getLng()));
    }
}
