package com.we.gilwe.utils;

import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.we.gilwe.BaseApplication;

/**
 * 토스트 메시지 헬퍼
 */
public class ToastHelper {


    public static void show(int resId) {
        show(BaseApplication.getContext().getString(resId));
    }

    public static void show(final String message) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    show(message);
                }
            });
        }

        Toast.makeText(BaseApplication.getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
