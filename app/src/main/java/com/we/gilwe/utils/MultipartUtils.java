package com.we.gilwe.utils;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by moltak on 2/24/17.
 */

public class MultipartUtils {

    public static RequestBody createPartFromString(String descriptionString) {
        if ("".equals(descriptionString)) return null;

        return RequestBody.create(okhttp3.MultipartBody.FORM, descriptionString);
    }

    public static MultipartBody.Part createImagePart(String path) {
        if ("".equals(path)) return null;

        File file = new File(path);
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/png"), file);
        return MultipartBody.Part.createFormData("image", file.getName(), requestFile);
    }
}
