package com.we.gilwe.utils;

import java.io.Closeable;

public final class IOUtils {
    private IOUtils() {
    }

    public static void close(Closeable closeable) {
        if(closeable != null) {
            try {
                closeable.close();
            } catch (Exception var2) {
                ;
            }
        }

    }
}

