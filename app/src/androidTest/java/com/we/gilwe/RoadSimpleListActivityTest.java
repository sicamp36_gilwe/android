package com.we.gilwe;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.we.gilwe.ui.road.RoadSimpleListActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by moltak on 2/24/17.
 */

@RunWith(AndroidJUnit4.class)
public class RoadSimpleListActivityTest {
    @Rule
    public ActivityTestRule<RoadSimpleListActivity> activityTestRule
            = new ActivityTestRule<>(RoadSimpleListActivity.class);

    @Test
    public void justExecute() throws InterruptedException {
        activityTestRule.getActivity();
        Thread.sleep(6000);
    }
}
